--[[
	Virtual File System Mount Order
	Can be path to 'data' or zip containing contents of 'data'
--]]

-- plugin = { path , mountPoint, relative }
return {
	["core"]			= { [[data/]], "/", true},
	["editor"] 			= { [[editor/data/]], "/", true }
}