local pdata = nil
local gfx = core.framework.graphics
function editor_toolbar_init(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_toolbar") then
		local w,h = core.framework.graphics.getSize()
		p.w = w
		pdata = data
		CallbackSet("framework_mousepressed",editor_toolbar_mousepressed)
	elseif (node.id == "toggle_editor_win") then
		data.__info.ui_editor_win = p.active
	elseif (node.id == "toggle_2d_mode") then
		p.active = editMode == "2d"
		if (data.toggle_3d_mode) then
			data.toggle_3d_mode.active = not p.active
		end
	elseif (node.id == "toggle_3d_mode") then
		p.active = editMode == "3d"
		if (data.toggle_2d_mode) then
			data.toggle_2d_mode.active = not p.active
		end
	end
end

function editor_toolbar_close(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_toolbar") then
		CallbackUnset("framework_mousepressed",editor_toolbar_mousepressed)
	end
end

function editor_toolbar_action(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_toolbar") then

	elseif (node.id == "toggle_editor_win") then
		data.__info.ui_editor_win = p.active
	elseif (node.id == "toggle_2d_mode") then
		editMode = "2d"
		if (data.toggle_3d_mode) then
			data.toggle_3d_mode.active = not p.active
		end
	elseif (node.id == "toggle_3d_mode") then
		editMode = "3d"
		if (data.toggle_2d_mode) then
			data.toggle_2d_mode.active = not p.active
		end
	end
end

function editor_toolbar_mousepressed(x,y,button,istouch)
	if (button == 1) then
		if (pdata.toggle_add_entity_mode and pdata.toggle_add_entity_mode.active) then
			local ent = selectedPrefab and core.entity.load(selectedPrefab) or core.entity.create("persistable","spatial")
			if (ent.component.spatial) then
				if (pdata.toggle_2d_mode and pdata.toggle_2d_mode.active) then
				
					-- Align to grid option
					if (pdata.toggle_2d_mode_snap_grid and pdata.toggle_2d_mode_snap_grid.active) then
						x = math.floor(x/editor.grid2DWidth) * editor.grid2DWidth
						y = math.floor(y/editor.grid2DHeight) * editor.grid2DHeight
					end
	
					-- If entity has a sprite, then position by top-left corner of the image
					if (ent.component.sprite) then
						local dx,dy = ent.component.sprite:getTextureSize()
						x = x + dx/2
						y = y + dy/2
					end
					
					ent.component.spatial:position(x,y,0)
					ent:addComponent("collision2D")
				else
					local camera = core.camera.getActive3D()
					if (camera) then
						local posX,posY,posZ,dirX,dirY,dirZ = camera:rayFromMouse()
						-- TODO: Also test for terrain intersect once implemented
						local x,y,z = core.framework.intersect.ray_plane(posX,posY,posZ,dirX,dirY,dirZ,0,0,0,0,1,0)
						if (x) then
							ent.component.spatial:position(x,y,z)
						end
					end
				end
			end
			selectedID = ent.id
		end
	end
end

function editor_toolbar_add_entity_mode_prefab_options(ctx,node,data,depth)
	-- TODO: Optimize
	local p = data.editor_toolbar_add_entity_mode_context_menu_prefabs
	if not (p.__node) then
		p.__node = {}
	end
	local function on_execute(path,fname,fullpath)
		-- Default data and fetch sprite data from entity file [TODO: obviously render model when implemented and render-to-texture implemented]
		if not (p.__node[fname]) then
			p.__node[fname] = {id="editor_toolbar_add_entity_mode_prefab_options"..fname,flags="NK_TEXT_ALIGN_LEFT",__content=fname}
			if not (p.__node[fname].__prefetch) then
				p.__node[fname].__prefetch = true
				local str,len = filesystem.read(fullpath)
				if (len and len > 0) then
					local status,result = xpcall(assert(loadstring(str,fullpath:sub(-45))),STP.stacktrace)
					if (status and result) then
						if (result.component.sprite and result.component.sprite.path) then
							p.__node[fname].image = result.component.sprite.path
						elseif (result.component.visual and result.component.visual.path) then
							p.__node[fname].render_wait = gfx.import.model(result.component.visual.path,{"PreTransformVertices","OptimizeMeshes","CalcTangentSpace","Triangulate","FlipUVs"})
						end
					else
						print(result,debug.traceback(1))
					end
				end
			end
		end
		if (p.__node[ fname ].render_wait) then
			local model = p.__node[ fname ].render_wait
			local ready = false
			for n,mes in ipairs(model.meshes) do
				for name,texture in pairs(mes.textures) do
					ready = texture.loaded
				end
			end
			if (ready and data[ p.__node[ fname ].id ]) then
				data[ p.__node[ fname ].id ].image = render_to_texture(model)
				p.__node[ fname ].render_wait = nil
			end
		end
		if (core.interface.api.select_label(ctx,p.__node[fname],data,depth) ~= nil) then
			selectedPrefab = fullpath
		end
		if (selectedPrefab ~= fullpath) then
			data[ p.__node[fname].id ].active = false
		end
	end
	filesystem.fileForEach("prefab/entity",true,{"ent"},on_execute)
end

function action_configure_grid(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "2d_mode_configure_grid") then
		data.__info.ui_grid_configure = true
	end
end

function render_to_texture(model)
	local framebuffer = gfx.import.framebuffer(1920,1080)
	gfx.setFramebuffer(framebuffer)
	gfx.clear()
		
	local mode = gfx.transform.getMatrixMode()
	gfx.transform.setMatrixMode("projection")
		gfx.transform.push()
			local mat4 = gfx.transform.getTransformation()
			local w, h = gfx.getSize()
			kazmath.kmMat4PerspectiveProjection(mat4,70,w/h,0.1,100)
			gfx.transform.setMatrixMode("model")
			gfx.transform.push()
				gfx.transform.translate(0,0.5,-2)
				gfx.transform.rotateZ(180*math.pi/180)
				model:draw()
			gfx.transform.pop()
		gfx.transform.pop()
	gfx.transform.setMatrixMode(mode)
	gfx.setFramebuffer()
	return framebuffer
end