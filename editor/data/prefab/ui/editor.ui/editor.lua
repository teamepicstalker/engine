local gfx 				= core.framework.graphics
local prefab 			= core.interface.import.prefab

preSelectedID			= nil				-- Currently highlighted entity in list
selectedID 				= nil				-- Selected entity
selectedPrefab			= nil				-- Path of selected prefab for Add Entity Mode
editMode				= "3d"				-- '2d' or '3d'

invoke("toolbar")
invoke("gridConfigure")
invoke("entityEditor")
invoke("entityComponentEditor")
invoke("worldEditor")

function editor_win_init(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "editor_win") then
		local w,h = gfx.getSize()
		p.y = data.editor_toolbar.y+data.editor_toolbar.h+1
		p.h = h-p.y
	elseif (node.id == "scene_color_background_r") then 
		local r,g,b,a = gfx.getBackgroundColor()
		p.value[0] = r/255
	elseif (node.id == "scene_color_background_g") then 
		local r,g,b,a = gfx.getBackgroundColor()
		p.value[0] = g/255
	elseif (node.id == "scene_color_background_b") then 
		local r,g,b,a = gfx.getBackgroundColor()
		p.value[0] = b/255
	elseif (node.id == "scene_color_background_a") then 
		local r,g,b,a = gfx.getBackgroundColor()
		p.value[0] = a/255
	elseif (node.id == "scene_color_background_pick") then
		local r,g,b,a = gfx.getBackgroundColor()
		local col = p.color[0]
		col.r = r/255
		col.g = g/255
		col.b = b/255
		col.a = a/255
	elseif (node.id == "scene_color_background_combo") then
		local r,g,b,a = gfx.getBackgroundColor()
		local col = p.color[0]
		col.r = r
		col.g = g
		col.b = b
		col.a = a
	elseif (node.id == "scene_tree_check_fullscreen") then 
		local flags = SDL.SDL_GetWindowFlags(core.framework.window._window)
		if (bit.band(flags,SDL.SDL_WINDOW_FULLSCREEN) ~= 0) then
			p.active = true
		else 
			p.active = false
		end
	elseif (node.id == "scene_tree_combo_display_modes") then
		local w,h = gfx.getSize()
		p.selected = w.."x"..h
	elseif (node.id == "scene_tree_combo_msaa") then
		local samples = ffi.new("GLint[1]")
		SDL.SDL_GL_GetAttribute(SDL.SDL_GL_MULTISAMPLESAMPLES,samples)
		p.selected = tostring(samples[0])
	end
end

function editor_win_update(ctx,node,data,depth)

end

function editor_win_close(ctx,node,data,depth)

end 

function action_scene_import_export(ctx,node,data,depth)
	if (prefab.exists("openScene") or prefab.exists("saveScene")) then 
		return
	end
	if (node.id == "menu_file_tree_scene_import") then
		local on_select = function(fullpath)
			core.entity.clear()
			editor.scene.load(fullpath)
		end
		prefab("openScene","prefab/ui/filebrowser.ui",{ directory="prefab/scene",filter={"scene"}, onSelect=on_select })
	elseif (node.id == "menu_file_tree_scene_export") then
		local on_select = function(fullpath)
			editor.scene.save(fullpath)
		end
		prefab("saveScene","prefab/ui/filebrowser.ui",{ directory="prefab/scene",filter={"scene"}, browserType="SaveAs", onSelect=on_select })
	elseif (node.id == "menu_file_tree_scene_new") then
		-- TODO: confirmation popup
		core.entity.clear()
	end
end 

function entity_tree_view(ctx,node,data,depth)
	nuklear.nk_layout_row_dynamic(ctx,25,1)
	local preSelected = nil
	for id,ent in pairs(core.entity.all()) do
		if (nuklear.nk_contextual_item_label(ctx,strformat("%s: %s",id,ent.alias),nuklear.NK_TEXT_LEFT) == 1) then
			data.__info.ui_entity_editor = true
			selectedID = id
		end
		-- ID for context menu to act upon
		if (preSelected == nil and bit.band(ctx.last_widget_state,nuklear.NK_WIDGET_STATE_HOVERED) ~= 0) then
			preSelected = id
		end
	end
	-- If context menu is already active, then do not change selected
	if not (data.scene_tree_entity_context_menu and data.scene_tree_entity_context_menu.isActive) then
		if (preSelected) then
			preSelectedID = preSelected
			data.__info.entity_tree_view_show_edit_remove = true
		else
			preSelectedID = nil
			data.__info.entity_tree_view_show_edit_remove = false
		end
	end
end

function action_add_edit_remove_entity(ctx,node,data,depth)
	local p = data[node.id]
	if (node.id == "scene_tree_entity_btn_add") then
		core.entity.create("persistable")
	elseif (node.id == "scene_tree_entity_context_menu_edit") then
		local ent = preSelectedID and core.entity.byID(preSelectedID)
		if (ent) then
			data.__info.ui_entity_editor = true
			selectedID = ent.id
		end
	elseif (node.id == "scene_tree_entity_context_menu_remove") then
		local ent = preSelectedID and core.entity.byID(preSelectedID)
		if (ent) then
			core.entity.destroy(ent.id)
			if (preSelectedID == selectedID) then 
				selectedID = nil
				data.__info.ui_entity_editor = nil
				data.__info.ui_entity_component_editor = nil
				data.editor_entity_editor_win = nil
				data.tree_components = nil
			end
		end
	elseif (node.id == "scene_tree_entity_context_menu_export") then
		local ent = preSelectedID and core.entity.byID(preSelectedID)
		if (ent) then
			if (core.interface.import.prefab.exists("saveEntity")) then 
				return
			end
			selectedID = preSelectedID
			local on_select = function(fullpath)
				local new_ent = selectedID and core.entity.byID(selectedID)
				if (new_ent) then
					core.entity.save(fullpath,new_ent)
				end
			end
			core.interface.import.prefab("saveEntity","prefab/ui/filebrowser.ui",{ directory="prefab/entity",filter={"ent"}, browserType="SaveAs", onSelect=on_select })
		end
	elseif (node.id == "scene_tree_entity_btn_import") then
		local on_select = function(fullpath)
			core.entity.load(fullpath)
		end
		core.interface.import.prefab("loadEntity","prefab/ui/filebrowser.ui",{ directory="prefab/entity",filter={"ent"}, onSelect=on_select })	
	end
	preSelectedID = nil
end

function scene_color_background_pick_action(ctx,node,data,depth)
	local r,g,b,a = gfx.getBackgroundColor()
	if (node.id == "scene_color_background_pick") then
		local col = data.scene_color_background_pick.color[0]
		r,g,b,a = col.r*255,col.g*255,col.b*255,col.a*255
	elseif (node.id == "scene_color_background_r") then
		r = data.scene_color_background_r.value[0]*255
	elseif (node.id == "scene_color_background_g") then
		g = data.scene_color_background_g.value[0]*255
	elseif (node.id == "scene_color_background_b") then
		b = data.scene_color_background_b.value[0]*255
	elseif (node.id == "scene_color_background_a") then
		a = data.scene_color_background_a.value[0]*255
	end
	col = data.scene_color_background_combo.color[0]
	col.r = r
	col.g = g
	col.b = b
	col.a = a
	core.framework.graphics.setBackgroundColor(r,g,b,a)
end

function camera_position(ctx,node,data,depth)
	local p = data.scene_tree_camera
	if not (p.__node) then
		p.__node = {}
	end
	
	for typ,t in pairs(core.camera.cache) do
		if (nuklear.nk_tree_push_hashed(ctx,nuklear.NK_TREE_TAB,typ,nuklear.NK_MINIMIZED,typ,#typ,0) == 1) then 
			local camera = core.camera["getActive"..typ]()
			if (camera) then
				nuklear.nk_layout_row_dynamic(ctx,25,1)
				for alias,camera in pairs(core.camera.cache[camera.type]) do
					if not (p.__node[alias]) then
						p.__node[alias] = {id="camera_tree_view_camera_list_label_"..alias,flags="NK_TEXT_ALIGN_LEFT",__content=alias}
					end
					if (core.interface.api.select_label(ctx,p.__node[alias],data,depth) ~= nil) then
						for aalias,ccamera in pairs(core.camera.cache[camera.type]) do
							data[p.__node[aalias].id].active = false
						end
						data[p.__node[alias].id].active = true
						core.camera["setActive"..camera.type](alias)
					end
					if (data[p.__node[alias].id] and camera == core.camera["getActive"..camera.type]()) then
						data[p.__node[alias].id].active = true
					end
				end
				
				nuklear.nk_layout_row_dynamic(ctx,135,1)
				if (nuklear.nk_group_begin(ctx,"Position",nuklear.NK_WINDOW_BORDER) == 1) then
					nuklear.nk_layout_row_dynamic(ctx,25,1)
					nuklear.nk_label(ctx,"Position:",nuklear.NK_TEXT_LEFT)
					local x,y,z = camera.position[1],camera.position[2],camera.position[3]
					x = nuklear.nk_propertyf(ctx, "#X:", -99999, x, 99999, 1, 1)
					y = nuklear.nk_propertyf(ctx, "#Y:", -99999, y, 99999, 1, 1)
					z = nuklear.nk_propertyf(ctx, "#Z:", -99999, z, 99999, 1, 1)
					camera.position[1],camera.position[2],camera.position[3] = x,y,z
					nuklear.nk_group_end(ctx)
				end
				if (nuklear.nk_group_begin(ctx,"Angle",nuklear.NK_WINDOW_BORDER) == 1) then
					nuklear.nk_layout_row_dynamic(ctx,25,1)
					nuklear.nk_label(ctx,"Angle:",nuklear.NK_TEXT_LEFT)
					local x,y,z = camera.angle[1],camera.angle[2],camera.angle[3]
					x = utils.wrap_minmax(nuklear.nk_propertyf(ctx, "#Pitch:", -361, x*180/math.pi, 361, 1,1)*math.pi/180,-2*math.pi,2*math.pi)
					y = utils.wrap_minmax(nuklear.nk_propertyf(ctx, "#Yaw:", -361, y*180/math.pi, 361, 1,1)*math.pi/180,-2*math.pi,2*math.pi)
					z = utils.wrap_minmax(nuklear.nk_propertyf(ctx, "#Roll:", -361, z*180/math.pi, 361, 1,1)*math.pi/180,-2*math.pi,2*math.pi)
					if (x ~= camera.angle[1] or y ~= camera.angle[2] or z ~= camera.angle[3]) then
						camera.angle[1],camera.angle[2],camera.angle[3] = x,y,z
						kazmath.kmQuaternionRotationPitchYawRoll(camera.orientation,x,y,z)
					end
					nuklear.nk_group_end(ctx)
				end
			end
			nuklear.nk_tree_pop(ctx)
		end
	end
end

function shader_properties(ctx,node,data,depth)
	for name,scriptEnv in pairs(core.framework.graphics.shaders.script) do
		if (scriptEnv.editor_controls) then
			if (nuklear.nk_tree_push_hashed(ctx,nuklear.NK_TREE_TAB,name,nuklear.NK_MINIMIZED,name,#name,0) == 1) then
				scriptEnv.editor_controls(ctx,node,content,data)
				nuklear.nk_tree_pop(ctx)
			end
		end
	end
end

function change_fullscreen(ctx,node,data,depth)
	local flags = SDL.SDL_GetWindowFlags(core.framework.window._window)
	if (bit.band(flags,SDL.SDL_WINDOW_FULLSCREEN) == SDL.SDL_WINDOW_FULLSCREEN) then 
		SDL.SDL_SetWindowFullscreen(core.framework.window._window,0)
		
		flags = bit.band(flags,bit.bnot(SDL.SDL_WINDOW_FULLSCREEN))
		core.config.w_value("window","flags",flags)
	else
		SDL.SDL_SetWindowFullscreen(core.framework.window._window,SDL.SDL_WINDOW_FULLSCREEN)
		
		flags = bit.bor(flags,SDL.SDL_WINDOW_FULLSCREEN)
		core.config.w_value("window","flags",flags)
	end
end

function get_display_modes(ctx,node,data,depth)
	nuklear.nk_layout_row_dynamic(ctx,25,1)
	
	local mode = ffi.new("SDL_DisplayMode")
	if (SDL.SDL_GetDesktopDisplayMode(0,mode) == 0) then
		-- error?
	end
	
	local modes = core.framework.window.getDisplayModes(0)
	for i,v in ipairs(modes) do
		if (v[4] == mode.refresh_rate) then
			if (nuklear.nk_contextual_item_label(ctx,v[1].."x"..v[2],nuklear.NK_TEXT_CENTERED) == 1) then
				data[node.id].selected = v[1].."x"..v[2]
				nuklear.nk_combo_close(ctx)
				core.framework.window.resize(v[1],v[2])
				
				core.config.w_value("window","width",v[1])
				core.config.w_value("window","height",v[2])
				
				break
			end
		end
	end
end

function get_msaa(ctx,node,data,depth)
	nuklear.nk_layout_row_dynamic(ctx,25,1)
	
	local pMaxSamples = ffi.new("GLint[1]")
	GL.glGetIntegerv(GL.GL_MAX_SAMPLES,pMaxSamples)
	
	local max_samples = math.min(16,pMaxSamples[0]) -- only because 32 looks buggy
	max_samples = 1/math.log(2,max_samples)
	
	for i=0,max_samples do
		local sample = i == 0 and 0 or 2^i
		if (nuklear.nk_contextual_item_label(ctx,tostring(sample),nuklear.NK_TEXT_CENTERED) == 1) then
			nuklear.nk_combo_close(ctx)
			
			SDL.SDL_GL_SetAttribute(SDL.SDL_GL_MULTISAMPLEBUFFERS,i == 0 and 0 or 1)
			SDL.SDL_GL_SetAttribute(SDL.SDL_GL_MULTISAMPLESAMPLES,sample)
			if (i == 0) then 
				GL.glDisable(GL.GL_MULTISAMPLE)
			else
				GL.glEnable(GL.GL_MULTISAMPLE)
			end
			
			data[node.id].selected = tostring(sample)
			
			core.config.w_value("window","msaa",sample)
			
			break
		end
	end
end
