local gfx 					= core.framework.graphics
local data					= nil				-- UI Storage

local pGrid 				= ffi.new("GLfloat[?]",120)
local pGrid2D				= ffi.new("GLfloat[?]",120)
local pAxis 				= ffi.new("GLfloat[?]",6)

grid2DWidth = 64
grid2DHeight = 64

function on_game_start()
	if (CommandLine[ "editor" ]) then 
		CallbackSet("framework_after_load",on_after_load)
		CallbackSet("framework_draw",on_draw,9999)
		CallbackSet("framework_update",on_update)
		CallbackSet("framework_keypressed",key_pressed)
		CallbackSet("framework_mousemoved",mousemoved)
		CallbackSet("framework_mousepressed",mousepressed)
		CallbackSet("framework_wheelmoved",wheelmoved)
		CallbackSet("framework_filedropped",filedropped)
		
		data = core.interface.import.prefab("editor","prefab/ui/editor.ui")
		data.__info.ui_active = true
	end
end

function on_after_load()
	-- Create new cameras and set them active
	local camera = core.camera("editor3D_000","prefab/camera/editor3D.camera")
	core.camera("editor3D_001","prefab/camera/editor3D.camera")
	core.camera("editor3D_002","prefab/camera/editor3D.camera")
	core.camera("editor2D","prefab/camera/editor2D.camera")
	core.camera.setActive3D("editor3D")
	core.camera.setActive2D("editor2D")
	
	camera.position[2] = 5
	camera.position[3] = 10
	camera:pitch(-20*math.pi/180)
		
	gfx.setBackgroundColor(100,100,100,255)
	
	-- Load a test Entity
	core.entity.load("prefab/entity/stalker.ent")
	
	-- Sound Stress Test!
	-- for i=1,100 do 
		-- this["test_music"..i] = core.framework.audio.import.sound("sound/oso_stalker_1.ogg")
		-- this["test_music"..i]:play(0,0,i,1,true)
	-- end
	
	-- Audio thread test! Make sure we successfully get device list!
	-- local t = core.framework.audio.getDeviceList()
	-- for i,v in pairs(t) do
		-- print(i,v)
	-- end
	
	local shader = gfx.shaders.script[ "GlTFPBR" ]
	if (shader) then
		shader.setLightColor(255,255,255)
	end
	
	local faces = {
		"right",
		"left",
		"top",
		"bottom",
		"front",
		"back"
	}

	local function getCubemap( filename, mipmapLevels )
		local t = {}
		for i = 0, mipmapLevels - 1 do
			for j = 1, #faces do
				local face = faces[ j ]
				local filename = string.format( filename, face, i )
				table.insert( t, { faces[ j ], filename } )
			end
		end
		return t
	end

	-- Create cube maps
	diffuseCubemap = core.framework.graphics.import.cubemap("diffuse",getCubemap("textures/cubemap/papermill/diffuse/diffuse_%s_%u.jpg",1))
	specularCubemap = core.framework.graphics.import.cubemap("specular",getCubemap("textures/cubemap/papermill/specular/specular_%s_%u.jpg",10))
end

function on_update(dt,mx,my)
	for name,scriptEnv in pairs(core.framework.graphics.shaders.script) do
		if (scriptEnv.editor_update) then
			scriptEnv.editor_update(dt,mx,my)
		end
	end
	
	local camera = core.camera.getActive3D()
	local mb3 = core.framework.mouse.pressed(2)
	-- Test entity update
	for id,ent in pairs(core.entity.all()) do 
		for name,c in pairs(ent.component) do
			if (c.update) then
				c:update(dt,mx,my)
			end
			
			-- Drag entity with middle mouse button if selected
			local selectedID = data.__scriptEnv and data.__scriptEnv.selectedID
			if (id == selectedID and mb3) then
				if (ent.component.visual and camera) then
					local posX,posY,posZ,dirX,dirY,dirZ = camera:rayFromMouse()
					local x,y,z = core.framework.intersect.ray_plane(posX,posY,posZ,dirX,dirY,dirZ,0,0,0,0,1,0)
					if (x) then
						ent.component.spatial:position(x,y,z)
					end
				elseif (ent.component.sprite) then
					ent.component.spatial:position(mx,my)
				end
			end
		end
	end
end

function on_draw(dt,mx,my)
	local camera = core.camera.getActive3D()
	if (camera) then
		gfx.shaders.useProgram("default3D")
		if (data.toggle_3d_mode and data.toggle_3d_mode.active) and (data.toggle_3d_mode_show_grid == nil or data.toggle_3d_mode_show_grid.active) then
			camera:push()
			
			local vao = gfx.getDefaultVAO()
			GL.glBindVertexArray(vao[0])
				
			local position = gfx.shaders.getAttribLocation("position")
			local normal = gfx.shaders.getAttribLocation("normal")
			local tangent = gfx.shaders.getAttribLocation("tangent")
			local bitangent = 3--gfx.shaders.getAttribLocation("bitangent")
			local texcoord = gfx.shaders.getAttribLocation("texcoord")
			
			GL.glEnableVertexAttribArray(position)
			GL.glDisableVertexAttribArray(normal)
			GL.glDisableVertexAttribArray(tangent)
			GL.glDisableVertexAttribArray(bitangent)
			GL.glDisableVertexAttribArray(texcoord)

			GL.glActiveTexture(GL.GL_TEXTURE3)
			GL.glBindTexture(GL.GL_TEXTURE_2D, gfx.getDefaultTexture()[0])
			
			gfx.transform.updateTransformations()
			
			-- 3D Grid
			for j=1,2 do
				local x,z,dx,dz = 0,0,0,0
				if (j == 1) then
					gfx.setColor(60,60,90,255)
					x = 1
					dx = 114
				else
					gfx.setColor(90,60,60,255)
					z = 1
					dz = 114
				end
				
				for i=0,119,6 do
					pGrid[0+i],pGrid[1+i],pGrid[2+i] = i*x-50,-1,i*z-50
					pGrid[3+i],pGrid[4+i],pGrid[5+i] = i*x+dz-50,-1,i*z+dx-50
				end
				
				local defaultVBO = gfx.getDefaultVBO()
				local size = ffi.sizeof(pGrid)

				GL.glBindBuffer(GL.GL_ARRAY_BUFFER, defaultVBO[0])
				GL.glBufferData(GL.GL_ARRAY_BUFFER, size, pGrid, GL.GL_STREAM_DRAW)
				GL.glVertexAttribPointer(position, 3, GL.GL_FLOAT, 0, 0, nil)

				gfx.drawArrays(GL.GL_LINES, 0, 120/3) -- 120/3
			end

			GL.glBindVertexArray(0)
			GL.glActiveTexture(GL.GL_TEXTURE0)

			if (data.scene_tree_camera_toggle_draw_frustum and data.scene_tree_camera_toggle_draw_frustum.active) then
				for k,v in pairs(core.camera.cache["3D"]) do
					v:drawFrustum()
				end
			end

			gfx.setColor(unpack(gfx.color.White))
			camera:pop()
		end
		
		camera:push()
		
		local mode = gfx.transform.getMatrixMode()
		gfx.transform.setMatrixMode("model")

		-- Test entity model draw
		for id,ent in pairs(core.entity.all()) do
			if (ent.component.visual) then
				--TODO: Simplify
				local tmin,tmax = ent.component.visual:getBoundingBox()
				local pos = ent.component.spatial:position()
				local rotm = ffi.new("kmMat4")
				local min = ffi.new("kmVec3",unpack(tmin))
				local max = ffi.new("kmVec3",unpack(tmax))
				kazmath.kmMat4RotationQuaternion(rotm,ent.component.spatial:orientation())
				kazmath.kmVec3MultiplyMat4(min,min,rotm)
				kazmath.kmVec3MultiplyMat4(max,max,rotm)
				if (camera:AABBInFrustum(pos[1]+min.x,pos[2]+min.y,pos[3]+min.z,pos[1]+max.x,pos[2]+max.y,pos[3]+max.z)) then
					local selectedID = data.__scriptEnv and data.__scriptEnv.selectedID
					ent.component.visual:draw()
					if (id == selectedID) then
						ent.component.visual:drawBBox()
					end
				end
			end
		end
			
		gfx.transform.setMatrixMode(mode)
		camera:pop()
	end
			
	camera = core.camera.getActive2D()
	if (camera) then
		gfx.shaders.useProgram("default2D")
		camera:push()
		
			-- Test entity sprite draw
			local mode = gfx.transform.getMatrixMode()
			gfx.transform.setMatrixMode("model")
			for id,ent in pairs(core.entity.all()) do 
				if (ent.component.sprite) then
					ent.component.sprite:draw()
				end
			end

			gfx.transform.setMatrixMode(mode)
			
		camera:pop()
		
		draw_grid2D()
	end
	
	local w,h = gfx.getSize()
	gfx.text("FPS: "..core.framework.getFPS(),w-120,h-50)
	gfx.text(strformat("SelectedID:%s",data.__scriptEnv and data.__scriptEnv.selectedID),w-250,h-50)
	
	draw_axis()
end

function draw_grid2D()
	if not (data.toggle_2d_mode and data.toggle_2d_mode.active) then
		return
	end
	
	camera = core.camera.getActive2D()
	if (camera) then
		local w,h = gfx.getSize()
		
		gfx.shaders.useProgram("default2D")
		
		local tex = gfx.shaders.getUniformLocation("tex")
		GL.glUniform1i(tex,0)
	
		local mode = gfx.transform.getMatrixMode()
		gfx.transform.setMatrixMode("view")
		camera:push()

		if (data.toggle_2d_mode_show_grid == nil or data.toggle_2d_mode_show_grid.active) then
			local vao = gfx.getDefaultVAO()
			GL.glBindVertexArray(vao[0])
				
			-- row
			if (grid2DWidth > 0) then
				for j=0,(w/grid2DWidth) do
					local x,y = 0,0
					gfx.setColor(200,200,200,255)
					
					pAxis[0],pAxis[1],pAxis[2] = x+j*grid2DWidth,0,0
					pAxis[3],pAxis[4],pAxis[5] = x+j*grid2DWidth,h,0
					
					local defaultVBO = gfx.getDefaultVBO()
					local size = ffi.sizeof(pAxis)
					local position = gfx.shaders.getAttribLocation("position")
					local texCoord = gfx.shaders.getAttribLocation("texcoord")
					local defaultTexture = gfx.getDefaultTexture()
					GL.glBindBuffer(GL.GL_ARRAY_BUFFER, defaultVBO[0])
					GL.glBufferData(GL.GL_ARRAY_BUFFER, size, pAxis, GL.GL_STREAM_DRAW)
					GL.glVertexAttribPointer(position, 3, GL.GL_FLOAT, 0, 0, nil)
					GL.glDisableVertexAttribArray(gfx.shaders.getAttribLocation("texcoord"))
					GL.glActiveTexture(GL.GL_TEXTURE0)
					GL.glBindTexture(GL.GL_TEXTURE_2D, defaultTexture[0])
					gfx.transform.updateTransformations()
					gfx.drawArrays(GL.GL_LINES, 0, 6/3) -- 6/3
					GL.glEnableVertexAttribArray(texCoord)
				end
			end
			-- col
			if (grid2DHeight > 0) then
				for j=0,(h/grid2DHeight) do
					local x,y = 0,0
					gfx.setColor(200,200,200,255)
					
					pAxis[0],pAxis[1],pAxis[2] = 0,y+j*grid2DHeight,0
					pAxis[3],pAxis[4],pAxis[5] = w,y+j*grid2DHeight,0

					local defaultVBO = gfx.getDefaultVBO()
					local size = ffi.sizeof(pAxis)
					local position = gfx.shaders.getAttribLocation("position")
					local texCoord = gfx.shaders.getAttribLocation("texcoord")
					local defaultTexture = gfx.getDefaultTexture()
					GL.glBindBuffer(GL.GL_ARRAY_BUFFER, defaultVBO[0])
					GL.glBufferData(GL.GL_ARRAY_BUFFER, size, pAxis, GL.GL_STREAM_DRAW)
					GL.glVertexAttribPointer(position, 3, GL.GL_FLOAT, 0, 0, nil)
					GL.glDisableVertexAttribArray(texCoord)
					GL.glActiveTexture(GL.GL_TEXTURE0)
					GL.glBindTexture(GL.GL_TEXTURE_2D, defaultTexture[0])
					gfx.transform.updateTransformations()
					gfx.drawArrays(GL.GL_LINES, 0, 6/3) -- 6/3
					GL.glEnableVertexAttribArray(texCoord)
				end
			end
			
			GL.glBindVertexArray(0)
		end
		
		-- 2D selected entity box
		local selectedID = data.__scriptEnv and data.__scriptEnv.selectedID
		if (selectedID) then
			local ent = core.entity.byID(selectedID)
			if (ent and ent.component.collision2D) then
				local x,y,w,h = ent.component.collision2D:boundingBox()
					
				gfx.setColor(100,255,100,255)
				
				local pAxis = ffi.new("GLfloat[?]",24)
				pAxis[0],pAxis[1],pAxis[2] = x,y,0
				pAxis[3],pAxis[4],pAxis[5] = x+w,y,0
				pAxis[6],pAxis[7],pAxis[8] = x+w,y+h,0
				pAxis[9],pAxis[10],pAxis[11] = x,y+h,0
				pAxis[12],pAxis[13],pAxis[14] = x,y,0
				pAxis[15],pAxis[16],pAxis[17] = x,y+h,0
				pAxis[18],pAxis[19],pAxis[20] = x+w,y,0
				pAxis[21],pAxis[22],pAxis[23] = x+w,y+h,0
				
				local vao = gfx.getDefaultVAO()
				GL.glBindVertexArray(vao[0])
				
				local defaultVBO = gfx.getDefaultVBO()
				local size = ffi.sizeof(pAxis)
				local position = gfx.shaders.getAttribLocation("position")
				local texCoord = gfx.shaders.getAttribLocation("texcoord")
				local defaultTexture = gfx.getDefaultTexture()
				GL.glBindBuffer(GL.GL_ARRAY_BUFFER, defaultVBO[0])
				GL.glBufferData(GL.GL_ARRAY_BUFFER, size, pAxis, GL.GL_STREAM_DRAW)
				GL.glVertexAttribPointer(position, 3, GL.GL_FLOAT, 0, 0, nil)
				GL.glDisableVertexAttribArray(texCoord)
				GL.glActiveTexture(GL.GL_TEXTURE0)
				GL.glBindTexture(GL.GL_TEXTURE_2D, defaultTexture[0])
				gfx.transform.updateTransformations()
				gfx.drawArrays(GL.GL_LINES, 0, 24/3) -- 12/3
				GL.glEnableVertexAttribArray(texCoord)
				
				GL.glBindVertexArray(0)
			end			
		end
		
		gfx.setColor(unpack(gfx.color.White))
		
		camera:pop()
		gfx.transform.setMatrixMode(mode)
	end
end

function draw_axis()
	local camera = core.camera.getActive3D()
	gfx.shaders.useProgram("default3D")
	
	local w,h = gfx.getSize()
	gfx.transform.setPerspectiveProjection(45,1,0.1,40)
	
 	local mode = gfx.transform.getMatrixMode()
	gfx.transform.setMatrixMode("view")
	gfx.transform.push()
	
	gfx.transform.lookAt(0,0,30,-10,11,0,0,1,0)
	gfx.transform.rotate(camera.orientation)
	gfx.transform.updateTransformations()
	
	local vao = gfx.getDefaultVAO()
	GL.glBindVertexArray(vao[0])

	local position = gfx.shaders.getAttribLocation("position")
	local normal = gfx.shaders.getAttribLocation("normal")
	local tangent = gfx.shaders.getAttribLocation("tangent")
	local bitangent = 3--gfx.shaders.getAttribLocation("bitangent")
	local texcoord = gfx.shaders.getAttribLocation("texcoord")
	
	GL.glEnableVertexAttribArray(position)
	GL.glDisableVertexAttribArray(normal)
	GL.glDisableVertexAttribArray(tangent)
	GL.glDisableVertexAttribArray(bitangent)
	GL.glDisableVertexAttribArray(texcoord)
	
	GL.glActiveTexture(GL.GL_TEXTURE3)
	GL.glBindTexture(GL.GL_TEXTURE_2D, gfx.getDefaultTexture()[0])
		
	for j=1,3 do
		local x,y,z = j==1 and 1 or 0, j==2 and 1 or 0, j==3 and 1 or 0
		local color = j==1 and gfx.color.Red or j==2 and gfx.color.Green or gfx.color.Blue
		gfx.setColor(unpack(color))
		
		pAxis[0],pAxis[1],pAxis[2] = 0,0,0
		pAxis[3],pAxis[4],pAxis[5] = x,y,z

		local defaultVBO = gfx.getDefaultVBO()
		local size = ffi.sizeof(pAxis)

		GL.glBindBuffer(GL.GL_ARRAY_BUFFER, defaultVBO[0])
		GL.glBufferData(GL.GL_ARRAY_BUFFER, size, pAxis, GL.GL_STREAM_DRAW)
		GL.glVertexAttribPointer(position, 3, GL.GL_FLOAT, 0, 0, nil)

		gfx.drawArrays(GL.GL_LINES, 0, 6/3) -- 6/3
	end
	
	GL.glBindVertexArray(0)
	GL.glActiveTexture(GL.GL_TEXTURE0)
	
 	gfx.transform.pop()
	gfx.transform.setMatrixMode(mode)
	gfx.setColor(unpack(gfx.color.White))
end 

function key_pressed(key,scancode,aliases,isrepeat)
	if (key == "F1") then
		data.__info.ui_active = not data.__info.ui_active
	end
	
	if (key == "E") then
		if (data.__info.ui_entity_editor) then 
			data.__info.ui_entity_editor = nil
			data.__info.ui_entity_component_editor = nil
			core.interface.api.clearData(data) -- to allow reinit
		else
			local selectedID = data.__scriptEnv and data.__scriptEnv.selectedID
			local ent = core.entity.byID(selectedID)
			if (ent) then
				data.__info.ui_entity_editor = true
			end
		end
	end

	-- Test entity input
	for id,ent in pairs(core.entity.all()) do 
		for name,c in pairs(ent.component) do
			if (c.key_pressed) then
				c:key_pressed(key,scancode,aliases,isrepeat)
			end
		end
	end
end

function mousepressed(x,y,button,istouch)
	if (data.toggle_2d_mode and data.toggle_2d_mode.active) then
		local cam = core.camera.getActive2D()
		if (cam and cam.mousepressed) then
			cam:mousepressed(x,y,button,istouch)
		end
	end
	if (data.toggle_3d_mode and data.toggle_3d_mode.active) then
		local cam = core.camera.getActive3D()
		if (cam and cam.mousepressed) then
			cam:mousepressed(x,y,button,istouch)
		end
	end
	
	if (button == 1) then
		for id,ent in pairs(core.entity.all()) do
			if (ent.component.collision2D and ent.component.collision2D:inside(x,y)) then
				data.__scriptEnv.selectedID = id
				break
			elseif (ent.component.visual) then
				local camera = core.camera.getActive3D()
				if (camera) then
					local posX,posY,posZ,dirX,dirY,dirZ = camera:rayFromMouse()
					local tmin,tmax = ent.component.visual:getBoundingBox()
					local pos = ent.component.spatial:position()
					local rotm = ffi.new("kmMat4")
					local min = ffi.new("kmVec3",unpack(tmin))
					local max = ffi.new("kmVec3",unpack(tmax))
					kazmath.kmMat4RotationQuaternion(rotm,ent.component.spatial:orientation())
					kazmath.kmVec3MultiplyMat4(min,min,rotm)
					kazmath.kmVec3MultiplyMat4(max,max,rotm)
					local pos = ent.component.spatial:position()
					local x,y,z,t = core.framework.intersect.ray_aabb(posX,posY,posZ,dirX,dirY,dirZ,pos[1]+min.x,pos[2]+min.y,pos[3]+min.z,pos[1]+max.x,pos[2]+max.y,pos[3]+max.z)
					if (x) then
						data.__scriptEnv.selectedID = id
						break
					end
				end
			end
		end
	end
end

function mousemoved(x,y,dx,dy,istouch)
	if (data.toggle_2d_mode and data.toggle_2d_mode.active) then
		local cam = core.camera.getActive2D()
		if (cam and cam.mousemoved) then
			cam:mousemoved(x,y,dx,dy,istouch)
		end
	end
	if (data.toggle_3d_mode and data.toggle_3d_mode.active) then
		local cam = core.camera.getActive3D()
		if (cam and cam.mousemoved) then
			cam:mousemoved(x,y,dx,dy,istouch)
		end
	end
end

function wheelmoved(dx,dy)
	if (data.toggle_2d_mode and data.toggle_2d_mode.active) then
		local cam = core.camera.getActive2D()
		if (cam and cam.wheelmoved) then
			cam:wheelmoved(dx,dy)
		end
	end
	if (data.toggle_3d_mode and data.toggle_3d_mode.active) then
		local cam = core.camera.getActive3D()
		if (cam and cam.wheelmoved) then
			cam:wheelmoved(dx,dy)
		end
	end
end

function filedropped(path)
	path = path:gsub("\\","/")
	
	for plugin,t in pairs(VFS_Plugins) do
		local a,b = path:find(t[1])
		if (a) then
			path = t[2] .. path:sub(b+1)
			break
		end
	end
	
	print(path)
	local ext = utils.get_ext(path)
	local fname = path:gsub("(.*[\\/])",""):gsub("(%..*)","")
	if (ext == "ui") then
		core.interface.import.prefab(fname,path)
	elseif (ext == "ent") then
		core.entity.load(path)
	elseif (ext == "scene") then
		core.scene.load(path)
	elseif (ext == "world") then
		core.world.load(path)
	elseif (ext == "camera") then
		core.camera(nil,path)
	elseif (ext == "ogf" or ext == "fbx" or ext == "obj" or ext == "3ds" or ext == "gltf" or ext == "dae") then
		local ent = core.entity.create("visual")
		ent.component.visual:model(path)
	elseif (ext == "dds") then
		local ent = core.entity.create("sprite")
		ent.component.sprite:image(path)
	end
end