--[[ 
	Scene Management
	The currently loaded scene
	
	A scene is a collection of entites and a unit of a 'World'
--]]

function save(fullpath)
	local sceneName = utils.trim_ext(utils.trim_path(fullpath))
	
	-- set current scene that entities exist in
	for id,ent in pairs(core.entity.all()) do
		ent.scene = sceneName
	end
	
	local data = {}
	core.entity.stateWrite(data)
	
	local old = filesystem.getWriteDirectory()
	filesystem.setWriteDirectory(filesystem.getBaseDirectory().."data\\")
	
	local str = "return "..print_table(data,true)
	filesystem.write(fullpath,str)
	
	filesystem.setWriteDirectory(old)
end 

function load(fullpath)
	local str,len = filesystem.read(fullpath)
	if (len == nil or len <= 0) then
		return
	end
	
	local status,result = xpcall(assert(loadstring(str,fullpath:sub(-45))),STP.stacktrace)
	if not (status) then
		print(result,debug.traceback(1))
		return
	end
	
	core.entity.stateRead(result)
end

-- To write scene's state to table, used by World
function stateWrite(data)
	core.entity.stateWrite(data)
end

-- To read scene's state from table, used by World
function stateRead(data)
	core.entity.stateRead(data)
end