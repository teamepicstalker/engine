--[[ 
	World Management
	
	A world is a collection of scenes and their entities. This additional layers allows the travel of entities
	between scenes, if necessary. This is what a new game will load as it's default state and will update to persist
	as a game save.
--]]

-- To save a world to file
function save(fullpath,newScenes)
	local data = {}
	
	-- combine scene data
	for name,v in pairs(newScenes) do
		data[name] = "prefab/scene/"..name..".scene"
	end
	
	local old = filesystem.getWriteDirectory()
	filesystem.setWriteDirectory(filesystem.getBaseDirectory().."data\\")
	
	local str = "return "..print_table(data,true)
	filesystem.write(fullpath,str)
	
	filesystem.setWriteDirectory(old)
end

-- To load a world from file
function load(fullpath)
	local str,len = filesystem.read(fullpath)
	if (len == nil or len <= 0) then
		return
	end
	
	local status,result = xpcall(assert(loadstring(str,fullpath:sub(-45))),STP.stacktrace)
	if not (status) then
		print(result,debug.traceback(1))
		return
	end
	
	core.entity.clear()
	
	for scene,path in pairs(result) do
		editor.scene.load(path)
	end
end