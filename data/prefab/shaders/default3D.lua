_shader 					= nil
local gfx = core.framework.graphics

function init(shader)
	gfx.shaders.setActive(shader)

	-- uniforms
	-- tex
	local tex = GL.glGetUniformLocation(shader,"tex")
	GL.glUniform1i(tex,3)
	GL.glActiveTexture(GL.GL_TEXTURE3)
	gfx.setDefaultTexture()
	GL.glActiveTexture(GL.GL_TEXTURE0)

	-- color
	local rgba = ffi.new("GLfloat[4]",1,1,1,1)
	local color  = GL.glGetUniformLocation(shader,"color")
	GL.glUniform4fv(color,1,rgba)
	
	-- attribs
	local position = GL.glGetAttribLocation(shader,"position")
	local normal = GL.glGetAttribLocation(shader,"normal")
	local tangent = GL.glGetAttribLocation(shader,"tangent")
	local bitangent = GL.glGetAttribLocation(shader,"bitangent")
	local texcoord = GL.glGetAttribLocation(shader,"texcoord")
	
	GL.glEnableVertexAttribArray(position)
	GL.glEnableVertexAttribArray(normal)
	GL.glEnableVertexAttribArray(tangent)
	GL.glEnableVertexAttribArray(bitangent)
	GL.glEnableVertexAttribArray(texcoord)
	
	local stride = (3+3+3+3+2)*ffi.sizeof("GLfloat")
	local vno = ffi.cast("GLvoid *",3*ffi.sizeof("GLfloat"))
	local vta = ffi.cast("GLvoid *",(3+3)*ffi.sizeof("GLfloat"))
	local vbi = ffi.cast("GLvoid *",(3+3+3)*ffi.sizeof("GLfloat"))
	local vte = ffi.cast("GLvoid *",(3+3+3+3)*ffi.sizeof("GLfloat"))
	
	GL.glVertexAttribPointer(position,3,GL.GL_FLOAT,0,stride,nil)
	GL.glVertexAttribPointer(normal,3,GL.GL_FLOAT,0,stride,vno)
	GL.glVertexAttribPointer(tangent,3,GL.GL_FLOAT,0,stride,vta)
	GL.glVertexAttribPointer(bitangent,3,GL.GL_FLOAT,0,stride,vbi)
	GL.glVertexAttribPointer(texcoord,2,GL.GL_FLOAT,0,stride,vte)
	
	-- uniforms
	-- model
	local model = GL.glGetUniformLocation(shader, "model")
	local mat4 = ffi.new("kmMat4")
	kazmath.kmMat4Identity(mat4)
	GL.glUniformMatrix4fv(model, 1, GL.GL_FALSE, mat4.mat)

	-- view
	local view = GL.glGetUniformLocation(shader, "view")
	GL.glUniformMatrix4fv(view, 1, GL.GL_FALSE, mat4.mat)

	-- projection
	local width, height = gfx.getSize()
	gfx.transform.setOrthographicProjection(width,height)
	local projection = GL.glGetUniformLocation(shader, "projection")
	GL.glUniformMatrix4fv(projection, 1, GL.GL_FALSE, mat4.mat)
end