_shader 					= nil
_brdfLUT 					= nil
_metallicRoughnessValues 	= ffi.new("GLfloat[2]",{1,1})
_cameraPosition				= ffi.new("GLfloat[3]",{0,0,0})
_lightDirection				= ffi.new("GLfloat[3]",{0,0,0})
_lightColor					= ffi.new("GLfloat[3]",{1,1,1})
_normalScale				= ffi.new("GLfloat[1]",1)
_emissiveFactor				= ffi.new("GLfloat[3]",{1,1,1})
_occlusionStrength			= ffi.new("GLfloat[1]",1)

local gfx = core.framework.graphics

function init(shader)
	gfx.shaders.setActive(shader)
	
	-- uniforms
	-- tex
	local tex = GL.glGetUniformLocation(shader,"tex")
	GL.glUniform1i(tex,3)
	GL.glActiveTexture(GL.GL_TEXTURE3)
	gfx.setDefaultTexture()
	GL.glActiveTexture(GL.GL_TEXTURE0)
	
	-- color
	local rgba = ffi.new("GLfloat[4]",1,1,1,1)
	local color  = GL.glGetUniformLocation(shader,"color")
	GL.glUniform4fv(color,1,rgba)
	
	-- uniforms
	-- u_LightDirection
	local u_LightDirection = GL.glGetUniformLocation(shader,"u_LightDirection")
	GL.glUniform3fv(u_LightDirection,1,_lightDirection)
	
	-- u_LightColor
	local u_LightColor = GL.glGetUniformLocation(shader,"u_LightColor")
	GL.glUniform3fv(u_LightColor,1,_lightColor)

	-- u_DiffuseEnvSampler
	local u_DiffuseEnvSampler = GL.glGetUniformLocation(shader,"u_DiffuseEnvSampler")
	GL.glUniform1i(u_DiffuseEnvSampler,1)
	
	-- u_SpecularEnvSampler
	local u_SpecularEnvSampler = GL.glGetUniformLocation(shader,"u_SpecularEnvSampler")
	GL.glUniform1i(u_SpecularEnvSampler,2)

	-- u_MetallicRoughnessSampler
	local u_MetallicRoughnessSampler = GL.glGetUniformLocation(shader,"u_MetallicRoughnessSampler")
	GL.glUniform1i(u_MetallicRoughnessSampler,4)

	-- u_NormalSampler
	local u_NormalSampler = GL.glGetUniformLocation(shader,"u_NormalSampler")
	GL.glUniform1i(u_NormalSampler,5)
	
	-- u_NormalScale
	local u_NormalScale = GL.glGetUniformLocation(shader,"u_NormalScale")
	GL.glUniform1fv(u_NormalScale,1,_normalScale)

	-- u_brdfLUT
	local u_brdfLUT = GL.glGetUniformLocation(shader,"u_brdfLUT")
	GL.glUniform1i(u_brdfLUT,6)
	_brdfLUT = gfx.import.image("textures/brdfLUT.png",{GL={{GL.GL_TEXTURE_WRAP_S,GL.GL_CLAMP_TO_EDGE},{GL.GL_TEXTURE_WRAP_T,GL.GL_CLAMP_TO_EDGE}}},setBrdfLUT)

	-- u_EmissiveSampler
	local u_EmissiveSampler = GL.glGetUniformLocation(shader,"u_EmissiveSampler")
	GL.glUniform1i(u_EmissiveSampler,7)
	
	-- u_EmissiveFactor
	local u_EmissiveFactor = GL.glGetUniformLocation(shader,"u_EmissiveFactor")
	GL.glUniform3fv(u_EmissiveFactor,1,_emissiveFactor)
	
	-- u_OcclusionSampler
	local u_OcclusionSampler = GL.glGetUniformLocation(shader,"u_OcclusionSampler")
	GL.glUniform1i(u_OcclusionSampler,8)
	
	-- u_OcclusionStrength
	local u_OcclusionStrength = GL.glGetUniformLocation(shader,"u_OcclusionStrength")
	GL.glUniform1fv(u_OcclusionStrength,1,_occlusionStrength)
	
	-- u_MetallicRoughnessValues
	local u_MetallicRoughnessValues = GL.glGetUniformLocation(shader,"u_MetallicRoughnessValues")
	GL.glUniform2fv(u_MetallicRoughnessValues,1,_metallicRoughnessValues)
	
	-- attribs
	-- attribs
	local position = GL.glGetAttribLocation(shader,"position")
	local normal = GL.glGetAttribLocation(shader,"normal")
	local tangent = GL.glGetAttribLocation(shader,"tangent")
	local bitangent = GL.glGetAttribLocation(shader,"bitangent")
	local texcoord = GL.glGetAttribLocation(shader,"texcoord")
	
	GL.glEnableVertexAttribArray(position)
	GL.glEnableVertexAttribArray(normal)
	GL.glEnableVertexAttribArray(tangent)
	GL.glEnableVertexAttribArray(bitangent)
	GL.glEnableVertexAttribArray(texcoord)
	
	local stride = (3+3+3+3+2)*ffi.sizeof("GLfloat")
	local vno = ffi.cast("GLvoid *",3*ffi.sizeof("GLfloat"))
	local vta = ffi.cast("GLvoid *",(3+3)*ffi.sizeof("GLfloat"))
	local vbi = ffi.cast("GLvoid *",(3+3+3)*ffi.sizeof("GLfloat"))
	local vte = ffi.cast("GLvoid *",(3+3+3+3)*ffi.sizeof("GLfloat"))
	
	GL.glVertexAttribPointer(position,3,GL.GL_FLOAT,0,stride,nil)
	GL.glVertexAttribPointer(normal,3,GL.GL_FLOAT,0,stride,vno)
	GL.glVertexAttribPointer(tangent,3,GL.GL_FLOAT,0,stride,vta)
	GL.glVertexAttribPointer(bitangent,3,GL.GL_FLOAT,0,stride,vbi)
	GL.glVertexAttribPointer(texcoord,2,GL.GL_FLOAT,0,stride,vte)
	
	-- uniforms
	-- model
	local model = GL.glGetUniformLocation(shader, "model")
	local mat4 = ffi.new("kmMat4")
	kazmath.kmMat4Identity(mat4)
	GL.glUniformMatrix4fv(model, 1, GL.GL_FALSE, mat4.mat)

	-- view
	local view = GL.glGetUniformLocation(shader, "view")
	GL.glUniformMatrix4fv(view, 1, GL.GL_FALSE, mat4.mat)

	-- projection
	local width, height = gfx.getSize()
	gfx.transform.setOrthographicProjection(width,height)
	local projection = GL.glGetUniformLocation(shader, "projection")
	GL.glUniformMatrix4fv(projection, 1, GL.GL_FALSE, mat4.mat)
end

function getLightDirection()
	return _lightDirection[0],_lightDirection[1],_lightDirection[2]
end

function setLightDirection(x,y,z)
	local shader = gfx.shaders.getActive()
	gfx.shaders.setActive(_shader)
	
	_lightDirection[0] = x or 0
	_lightDirection[1] = y or 0
	_lightDirection[2] = z or 0
	local index = GL.glGetUniformLocation(_shader,"u_LightDirection")
	GL.glUniform3fv(index,1,_lightDirection)
	
	gfx.shaders.setActive(shader)
end

function getLightColor()
	return _lightColor[0]*255,_lightColor[1]*255,_lightColor[2]*255
end

function setLightColor(r,g,b)
	local shader = gfx.shaders.getActive()
	gfx.shaders.setActive(_shader)
	
	_lightColor[0] = r/255
	_lightColor[1] = g/255
	_lightColor[2] = b/255
	local index = GL.glGetUniformLocation(_shader,"u_LightColor")
	GL.glUniform3fv(index,1,_lightColor)
	
	gfx.shaders.setActive(shader)
end

function getBrdfLUT()
	return _brdfLUT
end

function setBrdfLUT(filename)
	local shader = gfx.shaders.getActive()
	gfx.shaders.setActive(_shader)
	
	_brdfLUT = gfx.import.image(filename,{GL={{GL.GL_TEXTURE_WRAP_S,GL.GL_CLAMP_TO_EDGE},{GL.GL_TEXTURE_WRAP_T,GL.GL_CLAMP_TO_EDGE}}})
	
	GL.glActiveTexture(GL.GL_TEXTURE6)
	GL.glBindTexture(GL.GL_TEXTURE_2D,_brdfLUT.texture[0])
	GL.glActiveTexture(GL.GL_TEXTURE0)
	
	gfx.shaders.setActive(shader)
end

function getNormalScale()
	return _normalScale[0]
end

function setNormalScale(normalScale)
	local shader = gfx.shaders.getActive()
	gfx.shaders.setActive(_shader)
	
	_normalScale[0] = normalScale or 1
	local index = GL.glGetUniformLocation(_shader,"u_NormalScale")
	GL.glUniform1fv(index,1,_normalScale)
	
	gfx.shaders.setActive(shader)
end

function getEmissiveFactor()
	return _emissiveFactor[0],_emissiveFactor[1],_emissiveFactor[2]
end

function setEmissiveFactor(a,b,c)
	local shader = gfx.shaders.getActive()
	gfx.shaders.setActive(_shader)
	
	_emissiveFactor[0] = a or 1
	_emissiveFactor[1] = b or 1
	_emissiveFactor[2] = c or 1
	local index = GL.glGetUniformLocation(_shader,"u_EmissiveFactor")
	GL.glUniform3fv(index,1,_emissiveFactor)
	
	gfx.shaders.setActive(shader)
end

function getOcclusionStrength()
	return _occlusionStrength[0]
end

function setOcclusionStrength(occlusionStrength)
	local shader = gfx.shaders.getActive()
	gfx.shaders.setActive(_shader)
	
	_occlusionStrength[0] = occlusionStrength or 1
	local index = GL.glGetUniformLocation(_shader,"u_OcclusionStrength")
	GL.glUniform1fv(index,1,_occlusionStrength)
	
	gfx.shaders.setActive(shader)
end

function getMetallicRoughnessValues()
	return _metallicRoughnessValues[0],_metallicRoughnessValues[1]
end

function setMetallicRoughnessValues(a,b)
	local shader = gfx.shaders.getActive()
	gfx.shaders.setActive(_shader)
	
	_metallicRoughnessValues[0] = a or 1
	_metallicRoughnessValues[1] = b or 1
	local index = GL.glGetUniformLocation(_shader,"u_MetallicRoughnessValues")
	GL.glUniform2fv(index,1,_metallicRoughnessValues)
	
	gfx.shaders.setActive(shader)
end

function getCameraPosition()
	return _cameraPosition[0],_cameraPosition[1],_cameraPosition[2]
end

function setCameraPosition(x,y,z)
	local shader = gfx.shaders.getActive()
	gfx.shaders.setActive(_shader)
	
	_cameraPosition[0] = x or 0
	_cameraPosition[1] = y or 0
	_cameraPosition[2] = z or 0
	local index = GL.glGetUniformLocation(_shader,"u_Camera")
	GL.glUniform3fv(index,1,_cameraPosition)
	
	gfx.shaders.setActive(shader)
end

local bDontFollowMouse = 0
------------------------------------
-- Editor
------------------------------------
function editor_controls(ctx,element,node,content,data)
	-- LIGHT COLOR
	local name = "Light color"
	if (nuklear.nk_tree_push_hashed(ctx,nuklear.NK_TREE_TAB,name,nuklear.NK_MINIMIZED,name,#name,0) == 1) then
		local r,g,b = getLightColor()
		if not (eLightColorCombo) then
			eLightColorCombo = ffi.new("struct nk_color[1]")
		end
		eLightColorCombo[0].r = r or 0
		eLightColorCombo[0].g = g or 0
		eLightColorCombo[0].b = b or 0
		eLightColorCombo[0].a = 255
			
		local size = ffi.new("struct nk_vec2")
		size.x = 200
		size.y = 200
		nuklear.nk_layout_row_dynamic(ctx,35,1)
		if (nuklear.nk_combo_begin_color(ctx,eLightColorCombo[0],size) == 1) then
			if not (eLightColor) then
				eLightColor = ffi.new("struct nk_colorf[1]")
			end
			eLightColor[0].r = r/255 or 0
			eLightColor[0].g = g/255 or 0
			eLightColor[0].b = b/255 or 0
			eLightColor[0].a = 1
			nuklear.nk_layout_row_dynamic(ctx,125,1)
			if (nuklear.nk_color_pick(ctx,eLightColor,nuklear.NK_RGBA) == 1) then
				local r = eLightColor[0].r*255
				local g = eLightColor[0].g*255
				local b = eLightColor[0].b*255
				eLightColorCombo[0].r = r or 0
				eLightColorCombo[0].g = g or 0
				eLightColorCombo[0].b = b or 0
				setLightColor(r,g,b)
			end
			nuklear.nk_combo_end(ctx)
		end
		nuklear.nk_tree_pop(ctx)
	end
	-- LIGHT DIRECTION
	nuklear.nk_layout_row_dynamic(ctx,25,1)
	local size = ffi.new("struct nk_vec2")
	size.x = 300 -- w
	size.y = 200 -- h
	if (nuklear.nk_combo_begin_label(ctx,"Light direction",size) == 1) then
		nuklear.nk_layout_row_dynamic(ctx,25,1)
		local old = bDontFollowMouse
		bDontFollowMouse = nuklear.nk_check_label(ctx,"Follow Mouse",bDontFollowMouse)
		local x,y,z = getLightDirection()
		x = nuklear.nk_propertyf(ctx, "#X:", -99999, x, 99999, 0.1,0.1)
		y = nuklear.nk_propertyf(ctx, "#Y:", -99999, y, 99999, 0.1,0.1)
		z = nuklear.nk_propertyf(ctx, "#Z:", -99999, z, 99999, 0.1,0.1)
		setLightDirection(x,y,z)
		nuklear.nk_combo_end(ctx)
	end
	-- CAMERA
	if (nuklear.nk_combo_begin_label(ctx,"Normals Camera",size) == 1) then
		nuklear.nk_layout_row_dynamic(ctx,25,1)
		local x,y,z = getCameraPosition()
		x = nuklear.nk_propertyf(ctx, "#X:", -99999, x, 99999, 0.01,0.001)
		y = nuklear.nk_propertyf(ctx, "#Y:", -99999, y, 99999, 0.01,0.001)
		z = nuklear.nk_propertyf(ctx, "#Z:", -99999, z, 99999, 0.01,0.001)
		setCameraPosition(x,y,z)
		nuklear.nk_combo_end(ctx)
	end
	-- EMISSIVE FACTOR
	if (nuklear.nk_combo_begin_label(ctx,"Emissive factor",size) == 1) then
		nuklear.nk_layout_row_dynamic(ctx,25,1)
		local x,y,z = getEmissiveFactor()
		x = nuklear.nk_propertyf(ctx, "#R:", 0, x, 1, 0.01,0.001)
		y = nuklear.nk_propertyf(ctx, "#G:", 0, y, 1, 0.01,0.001)
		z = nuklear.nk_propertyf(ctx, "#B:", 0, z, 1, 0.01,0.001)
		setEmissiveFactor(x,y,z)
		nuklear.nk_combo_end(ctx)
	end
	-- METALLIC ROUGHNESS
	if (nuklear.nk_combo_begin_label(ctx,"Metallic roughness",size) == 1) then
		nuklear.nk_layout_row_dynamic(ctx,25,1)
		local x,y = getMetallicRoughnessValues()
		x = nuklear.nk_propertyf(ctx, "#X:", 0, x, 10, 0.01,0.01)
		y = nuklear.nk_propertyf(ctx, "#Y:", 0, y, 10, 0.01,0.01)
		setMetallicRoughnessValues(x,y)
		nuklear.nk_combo_end(ctx)
	end
	-- NORMAL SCALE
	local val = getNormalScale()
	val = nuklear.nk_propertyf(ctx, "#Normal scale:", 0, val, 10, 0.01,0.01)
	setNormalScale(val)
	-- OCCLUSION STRENTH
	val = getOcclusionStrength()
	val = nuklear.nk_propertyf(ctx, "#Occlusion strength:", 0, val, 10, 0.01,0.01)
	setOcclusionStrength(val)
end

function editor_update(dt,x,y)
	if (bDontFollowMouse == 0) then
		local w,h = gfx.getSize()
		local angleRad = math.atan2(h/2-y,x-w/2)
		
		local rotation = angleRad
		local pitch = math.rad(10)
		setLightDirection(
			math.sin( rotation ) * math.cos( pitch ),
			math.sin( pitch ),
			math.cos( rotation ) * math.cos( pitch )
		)
	end
	
	local camera = core.camera.getActive3D()
	if (camera) then
		local out = ffi.new("kmVec3")
		kazmath.kmQuaternionGetForwardVec3LH(out,camera.orientation)
		setCameraPosition(out.x,out.y,out.z)
	end
end