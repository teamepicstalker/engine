_shader 					= nil
local gfx = core.framework.graphics

function init(shader)
	gfx.shaders.setActive(shader)

	-- uniforms
	-- tex
	local tex = GL.glGetUniformLocation(shader,"tex")
	GL.glUniform1i(tex,0)
	
	-- color
	local rgba = ffi.new("GLfloat[4]",1,1,1,1)
	local color  = GL.glGetUniformLocation(shader,"color")
	GL.glUniform4fv(color,1,rgba)
	
	-- -- attribs
	-- local position = GL.glGetAttribLocation(shader,"position")
	-- local texcoord = GL.glGetAttribLocation(shader,"texcoord")
	-- local color = GL.glGetAttribLocation(shader, "color")
	
	-- GL.glEnableVertexAttribArray(position)
	-- GL.glEnableVertexAttribArray(texcoord)
	-- GL.glEnableVertexAttribArray(color)
	
	-- local stride = (4*ffi.sizeof("GLfloat"))+(4*ffi.sizeof("GLubyte"))
	-- local vt = ffi.cast("GLvoid *",(2)*ffi.sizeof("GLfloat"))
	-- local vc = ffi.cast("GLvoid *",(2+2)*ffi.sizeof("GLfloat"))

	-- GL.glVertexAttribPointer(position,2,GL.GL_FLOAT,0,stride,nil)
	-- GL.glVertexAttribPointer(texcoord,2,GL.GL_FLOAT,0,stride,vt)
	-- GL.glVertexAttribPointer(color,4,GL.GL_UNSIGNED_BYTE,0,stride,vc)
	
	-- -- uniforms
	-- -- model
	-- local model = GL.glGetUniformLocation(shader, "model")
	-- local mat4 = ffi.new("kmMat4")
	-- kazmath.kmMat4Identity(mat4)
	-- GL.glUniformMatrix4fv(model, 1, GL.GL_FALSE, mat4.mat)

	-- -- view
	-- local view = GL.glGetUniformLocation(shader, "view")
	-- GL.glUniformMatrix4fv(view, 1, GL.GL_FALSE, mat4.mat)

	-- -- projection
	-- local width, height = gfx.getSize()
	-- gfx.transform.setOrthographicProjection(width,height)
	-- local projection = GL.glGetUniformLocation(shader, "projection")
	-- GL.glUniformMatrix4fv(projection, 1, GL.GL_FALSE, mat4.mat)
end