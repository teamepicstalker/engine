_shader 					= nil

local gfx = core.framework.graphics

function init(shader)
	gfx.shaders.setActive(shader)
	
	-- uniforms
	-- tex
	local tex = GL.glGetUniformLocation(shader,"tex")
	GL.glUniform1i(tex,3)
	GL.glActiveTexture(GL.GL_TEXTURE3)
	gfx.setDefaultTexture()
	GL.glActiveTexture(GL.GL_TEXTURE0)
end