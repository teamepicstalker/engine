--[[
viewMatrix = {
	rightx,	righty,	rightz,	0
	upx,	upy,	upz,	0
	frontx,	fronty,	frontz,	0
	posx,	posy,	posz,	1
}
--]]
_activeCam3D			= nil		-- Currently active 3D camera
_activeCam2D			= nil		-- Currently active 3D camera
_mode					= nil

cache = { ["3D"] = {}, ["2D"] = {} }

local gfx = core.framework.graphics

function on_game_start()
	CallbackSet("framework_resize",on_resize)
end

function on_resize(w,h)
	for typ,t in pairs(cache) do
		for alias,camera in pairs(t) do
			if (camera.onWindowResize) then
				camera:onWindowResize(w,h)
			end
		end
	end
end

-- core.camera(alias,fullpath)
getmetatable(this).__call = function(self,alias,fullpath)
	local str,len = filesystem.read(fullpath)
	if (len == nil or len <= 0) then
		return
	end
	
	local status,result = xpcall(assert(loadstring(str,fullpath:sub(-45))),STP.stacktrace)
	if not (status) then
		print("[ERROR] failed to import camera " .. fullpath .. "\n" .. result)
		return
	end

	if not (cache[result.type]) then
		print("Camera type is undefined",alias,result.type)
		return
	end
	
	if not (alias) then
		local n = 1
		while (cache[result.type]["camera"..string.format("%03d",n)] ~= nil) do
			n = n + 1
		end
		alias = "camera"..string.format("%03d",n)
	end
	
	result.alias = alias
	
	if not (this["_activeCam"..result.type]) then 
		this["_activeCam"..result.type] = result
	end
	
	cache[result.type][alias] = result
	
	return result
end

function setActive3D(alias)
	_activeCam3D = cache["3D"][alias] or _activeCam3D
	assert(_activeCam3D,strformat("Camera %s doesn't exist",alias))
end  

function getActive3D()
	return _activeCam3D
end

function setActive2D(alias)
	_activeCam2D = cache["2D"][alias] or _activeCam2D
	assert(_activeCam2D,strformat("Camera %s doesn't exist",alias))
end  

function getActive2D()
	return _activeCam2D
end

function activeForEach(functor,...)
	if (_activeCam2D and _activeCam2D[functor]) then
		_activeCam2D[functor](_activeCam2D,...)
	end
	if (_activeCam3D and _activeCam3D[functor]) then
		_activeCam3D[functor](_activeCam3D,...)
	end
end