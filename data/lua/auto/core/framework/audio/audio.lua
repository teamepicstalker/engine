local thread = nil -- The main audio thread
linda = nil

function on_game_start()
	linda = lanes.linda()
	local function loop(linda)
		_G.linda = linda
		require("thread_audio")
	end
	thread = lanes.gen("*",loop)(linda)
	
	CallbackSet("framework_exit",on_game_exit)
end

function on_game_exit()
	thread:cancel(10)
end

-------------------------
-- Dummy Bit Stream
-------------------------
Class "cBitStreamDummy"
function cBitStreamDummy:__init(filename,isStreamed)
	linda:send("thread-call",{"newBitStream",{filename,isStreamed == true}})
	self.alias = filename
end

function cBitStreamDummy:play(x,y,z,volume,loop,relative,mandatory)
	linda:send("thread-call",{"playSound",{self.alias,x or 0,y or 0,z or 0,volume or 1,loop == true,relative == true, mandatory == true}})
	if (mandatory) then
		--- XXX: todo
	end
end

function cBitStreamDummy:stop()
	linda:send("thread-call",{"stopSound",{self.alias}})
end

-------------------------
-- API
-------------------------
function suspend()
	linda:send("thread-call","suspend")
end

function resume()
	linda:send("thread-call","resume")
end

function getDeviceList()
	linda:send("thread-call","getDeviceList")
	local k,v = linda:receive(30,"getDeviceList") -- Wait for results until timeout
	if (v ~= nil) then
		return v
	end
	return {}
end

function getCurrentDevice()
	linda:send("thread-call","getCurrentDevice")
	local k,v = linda:receive(30,"getCurrentDevice") -- Wait for results until timeout
	if (v ~= nil) then
		return v
	end
	return {}
end

function getCaptureDeviceList()
	-- XXX: unimplemented
end

local listener = {} -- To avoid constant table creation
function updateListener(x,y,z,vx,vy,vz,m4,m5,m6,m8,m9,m10)
	listener[1] = x
	listener[2] = y
	listener[3] = z
	listener[4] = vx
	listener[5] = vy
	listener[6] = vz
	listener[7] = m4
	listener[8] = m5
	listener[9] = m6
	listener[10] = m8
	listener[11] = m9
	listener[12] = m10
	linda:set("updateListener",listener)
end