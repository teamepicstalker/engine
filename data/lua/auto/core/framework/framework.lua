--[[
	Framework core
	
	TODO:
	1. Developer console w/ command system
	2. Mesh animations
	3. Skybox
	4. Scene system 
	5. Entity system
	6. Physics (Should 2D and 3D be separate or single lib that can handle both?)
--]]
local _frames        	= 0
local _lastTime      	= 0
local _fps           	= 0
local _nextFPSUpdate 	= 0
local _lastFPSUpdate 	= 0
local _event 			= nil
local _delta			= 0
disableInput			= false		-- Flag to temporarily disable input for one frame

function getFPS()
	return _fps
end

function getTime()
	return SDL.SDL_GetTicks()
end

function sleep(sec)
	SDL.SDL_Delay(sec*1000)
end

function step()
	_frames = _frames + 1

	local time = getTime()
	local dt = time - _lastTime
	_lastTime = time

	if (_nextFPSUpdate <= time) then
		_fps = _frames
		_nextFPSUpdate = time + (1*1000)
		_lastFPSUpdate = time
		_frames = 0
	end

	return dt
end

function run()
	-- Load configuration
	core.config.load()
	
	-- On Game Start callback
 	local function on_execute(path,fname,fullpath)
		-- Split paths and create tables for each directory
		local node = _G
		local namespace = "_G"
		local pp = path:sub(10) -- remove lua/auto/
		--for s in path:sub(10):gsplit("/",true) do
		for s in pp:gmatch("([^/]+)") do
			if (s ~= "") then
				if (node[ s ]) then 
					node = node[ s ]
					namespace = s
				end
			end
		end
		
		local sname = fname:sub(1,-5)
		if (sname ~= namespace) then
			node = node[sname]
		end

		if (node and node.on_game_start) then 
			node.on_game_start()
		end
	end
	filesystem.fileForEach("lua/auto",true,{"lua"},on_execute)
	
	-- Save configuration 
	core.config.save()
	
	-- Create window
	core.framework.window.create()
	core.framework.graphics.createDefaultVAO()
	core.framework.graphics.createDefaultVBO()
	core.framework.graphics.createDefaultEBO()
	
	CallbackSend("framework_load")
	CallbackSend("framework_after_load")
	
	-- for speed
	local origin = core.framework.graphics.transform.origin
	local clear = core.framework.graphics.clear
	local swap = core.framework.window.swap
	
	-- Main game loop
	while (true) do
		CallbackSend("framework_before_polling")
		local e = nil
		repeat
			e = poll()
			if (e) then
				if (e.type == ffi.C.SDL_QUIT or e.type == ffi.C.SDL_APP_TERMINATING) then
					if (quit()) then
						CallbackSend("framework_exit")
						core.config.save()
						filesystem.deinit()
						return
					end
				end
				CallbackSend("framework_event_polling",e)
				handle(e)
 			end
		until (e == nil)
		CallbackSend("framework_after_polling")

		local mouse_x,mouse_y = mouse.position()
		
		origin()
		
		_delta = step()
		CallbackSend("framework_update",_delta,mouse_x,mouse_y)

		clear()
		
 		CallbackSend("framework_draw",_delta,mouse_x,mouse_y)
		CallbackSend("framework_draw_end",_delta,mouse_x,mouse_y)
		
		swap()

		collectgarbage("step")
		sleep(0.001)
	end
end

function quit()
	return true
end

function visible(visible)

end

function move(x,y)

end

function resize(width,height)
	CallbackSend("framework_resize",width,height)
end

function minimize()

end

function maximize()

end

function restore()

end

function mousefocus(focus)

end

function focus(focus)

end

function poll()
	_event = _event or ffi.new("SDL_Event")
	return SDL.SDL_PollEvent(_event) ~= 0 and _event or nil
end

function handle(e)
	local ffi_C = ffi.C
	
	if (e.type == ffi_C.SDL_APP_LOWMEMORY) then
		CallbackSend("framework_lowmemory")
		collectgarbage()
		collectgarbage()
		return
	elseif (e.type == ffi_C.SDL_WINDOWEVENT) then
		if (e.window.event == ffi_C.SDL_WINDOWEVENT_SHOWN) then
			visible(true)
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_HIDDEN) then
			visible(false)
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_MOVED) then
			move(e.window.data1, e.window.data2)
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_RESIZED) then
			resize(e.window.data1, e.window.data2)
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_SIZE_CHANGED) then
			resize(e.window.data1, e.window.data2)
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_MINIMIZED) then
			minimize()
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_MAXIMIZED) then
			maximize()
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_RESTORED) then
			restore()
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_ENTER) then
			mousefocus(true)
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_LEAVE) then
			mousefocus(false)
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_FOCUS_GAINED) then
			focus(true)
		elseif (e.window.event == ffi_C.SDL_WINDOWEVENT_FOCUS_LOST) then
			focus(false)
		end
		return
	elseif (e.type == ffi_C.SDL_CLIPBOARDUPDATE) then
		CallbackSend("framework_clipboard",ffi.string(e.drop.file))
		return
	elseif (e.type == ffi_C.SDL_DROPFILE) then
		CallbackSend("framework_filedropped",ffi.string(e.drop.file))
		return
	elseif (e.type == ffi_C.SDL_DROPTEXT) then
		return
	elseif (e.type == ffi_C.SDL_DROPBEGIN) then
		return
	elseif (e.type == ffi_C.SDL_DROPCOMPLETE) then
		return
	elseif (e.type == ffi_C.SDL_AUDIODEVICEADDED) then
		return
	elseif (e.type == ffi_C.SDL_AUDIODEVICEREMOVED) then
		return
	elseif (e.type == ffi_C.SDL_JOYDEVICEADDED) then
		local index = e.jdevice.which
		local GUID = ffi.new("char[?]",33)
		SDL.SDL_JoystickGetGUIDString(SDL.SDL_JoystickGetDeviceGUID(index),GUID,33)
		local guid = ffi.string(GUID)
		if (core.config.data.keybinding.joystick and core.config.data.keybinding.joystick[ guid ]) then
			joystick.aliases[ guid ] = core.config.data.keybinding.joystick[ guid ]
		else 
			joystick.aliases[ guid ] = {}
			core.config.data.keybinding.joystick[ guid ] = joystick.aliases[ guid ]
			for alias,t in pairs(core.config.data.keybinding.joystick.default) do 
				joystick.aliases[ guid ][ alias ] = {t[ 1 ],t[ 2 ]}
				if not (joystick.joyToAlias[ guid ]) then 
					joystick.joyToAlias[ guid ] = {}
				end
				if not (joystick.joyToAlias[ guid ][ t[ 1 ] ]) then 
					joystick.joyToAlias[ guid ][ t[ 1 ] ] = {}
				end
				joystick.joyToAlias[ guid ][ t[ 1 ] ][ alias ] = true
			end
		end
		return
	elseif (e.type == ffi_C.SDL_JOYDEVICEREMOVED) then
		--[[
		local index = e.jdevice.which
		local GUID = ffi.new("char[?]",33)
		SDL.SDL_JoystickGetGUIDString(SDL.SDL_JoystickGetDeviceGUID(index),GUID,33)
		local guid = ffi.string(GUID)
		--]]
		return
	elseif (e.type == ffi_C.SDL_CONTROLLERDEVICEADDED) then
		return
	elseif (e.type == ffi_C.SDL_CONTROLLERDEVICEREMOVED) then
		return
	elseif (e.type == ffi_C.SDL_CONTROLLERDEVICEREMAPPED) then
		return
	end 
	
	if (disableInput) then
		disableInput = false
		return
	end
	
	if (e.type == ffi_C.SDL_KEYDOWN) then
		local key = SDL.SDL_GetKeyName(e.key.keysym.sym)
		key = ffi.string(key)
		local scancode = SDL.SDL_GetScancodeName(e.key.keysym.scancode)
		scancode = ffi.string(scancode)
		local isrepeat = e.key[ "repeat" ] == 1 and true or false
		CallbackSend("framework_keypressed",key,scancode,keyboard.keyToAlias[key],isrepeat)
	elseif (e.type == ffi_C.SDL_KEYUP) then
		local key = SDL.SDL_GetKeyName(e.key.keysym.sym)
		key = ffi.string(key)
		local scancode = SDL.SDL_GetScancodeName(e.key.keysym.scancode)
		scancode = ffi.string(scancode)
		CallbackSend("framework_keyreleased",key,scancode,keyboard.keyToAlias[key])
	elseif (e.type == ffi_C.SDL_TEXTEDITING) then
		CallbackSend("framework_textedited",e.edit.text, e.edit.start, e.edit.length)
	elseif (e.type == ffi_C.SDL_TEXTINPUT) then
		CallbackSend("framework_textinput",e.text.text)
	elseif (e.type == ffi_C.SDL_MOUSEMOTION) then
		local x = e.motion.x
		local y = e.motion.y
		local dx = e.motion.xrel
		local dy = e.motion.yrel
		local istouch = e.motion.which == SDL.SDL_TOUCH_MOUSEID
		CallbackSend("framework_mousemoved",x,y,dx,dy,istouch)
	elseif (e.type == ffi_C.SDL_MOUSEBUTTONDOWN) then
		local x = e.button.x
		local y = e.button.y
		local button = e.button.button
		local istouch = e.button.which == SDL.SDL_TOUCH_MOUSEID
		CallbackSend("framework_mousepressed",x,y,button,istouch)
	elseif (e.type == ffi_C.SDL_MOUSEBUTTONUP) then
		local x = e.button.x
		local y = e.button.y
		local button  = e.button.button
		if (button == 2) then
			button = 3
		elseif (button == 3) then
			button = 2
		end
		local istouch = e.button.which == SDL.SDL_TOUCH_MOUSEID
		CallbackSend("framework_mousereleased",x,y,button,istouch)
	elseif (e.type == ffi_C.SDL_MOUSEWHEEL) then
		CallbackSend("framework_wheelmoved",e.wheel.x, e.wheel.y)
	elseif (e.type == ffi_C.SDL_JOYAXISMOTION) then
		local index = e.jdevice.which
	elseif (e.type == ffi_C.SDL_JOYBALLMOTION) then
	elseif (e.type == ffi_C.SDL_JOYHATMOTION) then
	elseif (e.type == ffi_C.SDL_JOYBUTTONDOWN) then
		local index = e.jdevice.which
		local button = e.jdevice.button
		CallbackSend("framework_joypressed",index,button,joystick.joyToAlias[button])
	elseif (e.type == ffi_C.SDL_JOYBUTTONUP) then
		local index = e.jdevice.which
		local button = e.jdevice.button
		CallbackSend("framework_joyreleased",index,button,joystick.joyToAlias[button])
	elseif (e.type == ffi_C.SDL_CONTROLLERAXISMOTION) then
	elseif (e.type == ffi_C.SDL_CONTROLLERBUTTONDOWN) then
	elseif (e.type == ffi_C.SDL_CONTROLLERBUTTONUP) then
	elseif (e.type == ffi_C.SDL_FINGERDOWN) then
	elseif (e.type == ffi_C.SDL_FINGERUP) then
	elseif (e.type == ffi_C.SDL_FINGERMOTION) then
	elseif (e.type == ffi_C.SDL_DOLLARGESTURE) then
	elseif (e.type == ffi_C.SDL_DOLLARRECORD) then
	elseif (e.type == ffi_C.SDL_MULTIGESTURE) then
	end
end

function delta()
	return _delta
end