local expat = require("expat")	; if not (expat) then error("failed to load expat") end

-- core.framework.import.xml()
getmetatable(this).__call = function(self)
	return cXML()
end

-- table structure example:
--[[

<xml>
	<test id="one">
		<text>This is text</text>
	</test>
	<test id="two">
	</test>
	<test id="three">
		<some>
			<deep>
				<nest x="100" y="0" />
			</deep>
		</some>
	</test>
</xml>	
	print(xml.data.xml[1].test[1][1].id) 		 						=> one
	print(xml.data.xml[1].test[1].text[1][2])  						=> This is text
	print(xml.data.xml[1].test[3].some[1].deep[1].nest[1][1].x)		=> 100
	
	For a tag node table (ie. test[1]) , first index is always attributes table (ie. test[1][1]["id"])
	Second index is content. (ie. test[1].text[1][2])
--]]

Class "cXML"
function cXML:__init()

end

function cXML:parse(filename)
	self.data = {}
	
	local parser = expat.XML_ParserCreate(nil)
	local last_content -- text within the tags
	local node = self.data -- current node in the tree
	local tmp = {} -- To keep track of parent node
	local parent_by_node = {}
	
	local function trim(s)
		return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
	end

	local function start_element(data,element,attribute)
		local ele = trim(ffi.string(element))
		if not (node[ ele ]) then 
			node[ ele ] = {}
		end
		
		local new_node = {}
		table.insert(node[ ele ],new_node)
		new_node[1] = {} -- attributes
		
		local idx = 0
		while (attribute[idx] ~= nil and attribute[idx+1] ~= nil) do
			local key,val = trim(ffi.string(attribute[idx])), trim(ffi.string(attribute[idx+1]))
			new_node[1][key] = val
			if (self.attribute_callback) then
				self:attribute_callback(new_node,node,ele,key,val)
			end
			idx = idx + 2
		end
		
		parent_by_node[ new_node ] = node
		node = new_node
		
		if (self.start_element_callback) then
			self:start_element_callback(new_node,node,ele)
		end
	end
	
	local function end_element(data,element)
		local ele = trim(ffi.string(element))
		if (last_content) then
			node[2] = last_content
			last_content = nil
		end
		if (self.end_element_callback) then
			self:end_element_callback(node,parent_by_node[node],ele)
		end
		node = parent_by_node[node] or self.data
	end
	
	local function handle_data(data,content,len)
		if (content ~= nil) then 
			local str = trim(ffi.string(content,len))
			if (str ~= "") then 
				last_content = (last_content and last_content .. str) or str
			end
		end
	end
	
	expat.XML_SetElementHandler(parser,start_element,end_element)
	expat.XML_SetCharacterDataHandler(parser,handle_data)
	
	local buffer, length = filesystem.read(filename)
	if (length == nil or length == 0) then 
		print("core.framework.import.xml - Invalid path")
	elseif (expat.XML_Parse(parser, buffer, length, 1) == expat.XML_STATUS_ERROR) then
        print("core.framework.import.xml:", ffi.string(expat.XML_ErrorString(expat.XML_GetErrorCode(parser))))
	end
	
	expat.XML_ParserFree(parser)
	
	--print_table(self.data)
	
	return self
end