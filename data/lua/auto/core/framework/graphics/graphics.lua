-- TODO: Check for more memory leaks

local _color			= ffi.new("GLfloat[4]",0,0,0,0)
local _defaultVAO 		= nil 
local _defaultVBO 		= nil
local _defaultEBO		= nil
local _drawCalls 		= 0
local _font 			= nil
local _framebuffer 		= nil
local _defaultTexture	= nil
local _lineWidth		= 1
local _vertices			= {}
local _backgroundColor	= {0,0,0,255}
local _viewport			= {0,0,800,600}

local floor,sin,cos,pi = math.floor,math.sin,math.cos,math.pi

function setViewport(x,y,w,h)
	GL.glViewport(x,y,w,h)
	_viewport[1],_viewport[2],_viewport[3],_viewport[4] = x,y,w,h
end

function getViewport()
	return unpack(_viewport)
end

function createDefaultEBO()
	_defaultEBO = ffi.new("GLuint[1]")
	GL.glGenBuffers(1, _defaultEBO)
	GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, _defaultEBO[0])
	ffi.gc(_defaultEBO,function(buffer) GL.glDeleteBuffers(1,buffer) end)
end

function getDefaultEBO()
	return _defaultEBO
end

function createDefaultVAO()
	_defaultVAO = ffi.new("GLuint[1]")
	GL.glGenVertexArrays(1, _defaultVAO)
	GL.glBindVertexArray(_defaultVAO[0])
	ffi.gc(_defaultVAO,function(buffer) GL.glDeleteVertexArrays(1,buffer) end)
end

function getDefaultVAO()
	return _defaultVAO
end

function createDefaultVBO()
	_defaultVBO = ffi.new("GLuint[1]")
	GL.glGenBuffers(1, _defaultVBO)
	GL.glBindBuffer(GL.GL_ARRAY_BUFFER, _defaultVBO[0])
	ffi.gc(_defaultVBO,function(buffer) GL.glDeleteBuffers(1,buffer) end)
end

function getDefaultVBO()
	return _defaultVBO
end

function drawArrays(mode, first, count)
	GL.glDrawArrays(mode, first, count)
	_drawCalls = _drawCalls + 1
end

function drawElements(mode,count,offset)
	GL.glDrawElements(mode,count,GL.GL_UNSIGNED_INT,offset)
	_drawCalls = _drawCalls + 1
end

function getDrawCalls()
	return _drawCalls
end

function draw(drawable, x, y, r, sx, sy, ox, oy, kx, ky)
	x = x or 0
	y = y or 0
	drawable:draw(x, y, r, sx, sy, ox, oy, kx, ky)
end

function getFont()
	if not (_font) then 
		_font = core.framework.graphics.import.font("fonts/Vera.ttf",18)
	end
	return _font
end

function getFramebuffer()
	return _framebuffer
end

function getPolygonMode()
	local mode = ffi.new("GLint[1]")
	GL.glGetIntegerv(GL.GL_POLYGON_MODE, mode)
	if (mode[0] == GL.GL_LINE) then
		return "line"
	elseif(mode[0] == GL.GL_FILL) then
		return "fill"
	elseif (mode[0] == GL.GL_POINT) then
		return "point"
	end
end

function getSize()
	local width  = ffi.new("int[1]")
	local height = ffi.new("int[1]")
	SDL.SDL_GL_GetDrawableSize(core.framework.window._window, width, height)
	return width[0], height[0]
end

function text(txt, x, y, r, sx, sy, ox, oy, kx, ky)
	txt = tostring(txt)
	x = x or 0
	y = y or 0
	local font = getFont()
	font:text(txt, x, y, r, sx, sy, ox, oy, kx, ky)
end

function setFont(font)
	_font = font or getFont()
end

function setFramebuffer(framebuffer)
	if (framebuffer) then
		GL.glBindFramebuffer(GL.GL_FRAMEBUFFER, framebuffer.framebuffer[0])
	else
		GL.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0)
	end
	_framebuffer = framebuffer
end

function setPolygonMode(mode)
	if (mode == "line") then
		mode = GL.GL_LINE
	elseif (mode == "fill") then
		mode = GL.GL_FILL
	elseif (mode == "point") then
		mode = GL.GL_POINT
	end
	GL.glPolygonMode(GL.GL_FRONT_AND_BACK, mode)
end

function setVSync(vsync)
	SDL.SDL_GL_SetSwapInterval(vsync and 1 or 0)
end

function setBackgroundColor(r,g,b,a)
	_backgroundColor[1] = r or 0
	_backgroundColor[2] = g or 0
	_backgroundColor[3] = b or 0
	_backgroundColor[4] = a or 255
end

function getBackgroundColor()
	return _backgroundColor[1],_backgroundColor[2],_backgroundColor[3],_backgroundColor[4]
end

function clear()
	GL.glClearColor(_backgroundColor[1]/255,_backgroundColor[2]/255,_backgroundColor[3]/255,_backgroundColor[4]/255)
	GL.glClear(bit.bor(GL.GL_COLOR_BUFFER_BIT, GL.GL_DEPTH_BUFFER_BIT))
end

function setScissor(x,y,w,h)
	if not (x) then
		GL.glDisable(GL.GL_SCISSOR_TEST)
		return
	end 
	GL.glEnable(GL.GL_SCISSOR_TEST)
	local vw,vh = getSize()
	GL.glScissor(x,vh-y-h,w,h)
end

function getDefaultTexture()
	if not (_defaultTexture) then
		setDefaultTexture()
	end
	return _defaultTexture
end

function setDefaultTexture()
	if (_defaultTexture) then
		GL.glBindTexture(GL.GL_TEXTURE_2D, _defaultTexture[0])
		return
	end
	_defaultTexture = ffi.new("GLuint[1]")
	GL.glGenTextures(1, _defaultTexture)
	GL.glBindTexture(GL.GL_TEXTURE_2D, _defaultTexture[0])

	local pixels = ffi.new("GLfloat[4]", 1.0, 1.0, 1.0, 1.0)
	GL.glTexImage2D(
		GL.GL_TEXTURE_2D,
		0,
		GL.GL_RGBA,
		1,
		1,
		0,
		GL.GL_RGBA,
		GL.GL_FLOAT,
		pixels
	)
end

function getColor()
	return _color[0],_color[1],_color[2],_color[3]
end

function setColor(r,g,b,a)
	_color[0] = (r or 0) / 255
	_color[1] = (g or 0) / 255
	_color[2] = (b or 0) / 255
	_color[3] = (a or 255) / 255
	local index  = shaders.getUniformLocation("color")
	GL.glUniform4fv(index, 1, _color)
end

function setClearColor(r,g,b,a)
	r = r or 0
	g = g or 0
	b = b or 0
	a = a or 0
	GL.glClearColor(r/255,g/255,b/255,a)
end

function polygon(mode, vertices)
	if (mode == "line") then
		mode = GL.GL_LINE_LOOP
	elseif (mode == "fill") then
		mode = GL.GL_TRIANGLES
	end
	local defaultVBO     = _defaultVBO
	local pVertices      = ffi.new("GLfloat[?]", #vertices, vertices) -- XXX: I don't think pVertices is being garbage collected!
	local size           = ffi.sizeof(pVertices)
	
	local position       = shaders.getAttribLocation("position")
	local texCoord       = shaders.getAttribLocation("texcoord")
	
	GL.glEnableVertexAttribArray(position)
	GL.glDisableVertexAttribArray(texCoord)
	
	local defaultTexture = getDefaultTexture()
	GL.glBindBuffer(GL.GL_ARRAY_BUFFER, defaultVBO[0])
	GL.glBufferData(GL.GL_ARRAY_BUFFER, size, pVertices, GL.GL_STREAM_DRAW)
	GL.glVertexAttribPointer(position, 2, GL.GL_FLOAT, 0, 0, nil)
	
	GL.glBindTexture(GL.GL_TEXTURE_2D, defaultTexture[0])
	transform.updateTransformations()
	drawArrays(mode, 0, #vertices / 2)
end

function rectangle(mode,x,y,w,h,cornerRadius)
	if (cornerRadius and cornerRadius > 0) then
		mode = mode == "fill" and GL.GL_TRIANGLE_FAN or mode
		local segmentsPerCorner = floor(cornerRadius*pi*2/4)
		for i=#_vertices,1,-1 do 
			_vertices[#_vertices] = nil
		end
		_vertices[1] = x+w/2 
		_vertices[2] = y+h/2
		local angleDelta = 1/segmentsPerCorner*pi/2
		local cornerCenterVerts = {x+w-cornerRadius,y+h-cornerRadius,x+cornerRadius,y+h-cornerRadius,x+cornerRadius,y+cornerRadius,x+w-cornerRadius,y+cornerRadius}
		for corner=0,3 do
			local angle = corner*pi/2
			local cornerCenterX = cornerCenterVerts[corner*2+1]
			local cornerCenterY = cornerCenterVerts[corner*2+2]
			for s=1,segmentsPerCorner do 
				table.insert(_vertices,cornerCenterX+cos(angle)*cornerRadius)
				table.insert(_vertices,cornerCenterY+sin(angle)*cornerRadius)
				angle = angle + angleDelta
			end
		end
		_vertices[#_vertices+1] = x+w
		_vertices[#_vertices+1] = y+h-cornerRadius
	else
		for i=#_vertices,1,-1 do 
			_vertices[#_vertices] = nil
		end
		_vertices[1] = x
		_vertices[2] = y+h
		_vertices[3] = x+w
		_vertices[4] = y+h
		_vertices[5] = x
		_vertices[6] = y 
		_vertices[7] = x+w
		_vertices[8] = y+h
		_vertices[9] = x+w
		_vertices[10]= y
		_vertices[11]= x
		_vertices[12]= y
	end
	polygon(mode, _vertices)
end

-- XXX get working
function setLineWidth(lineWidth)
	_lineWidth = lineWidth
end

function getLineWidth()
	return _lineWidth
end