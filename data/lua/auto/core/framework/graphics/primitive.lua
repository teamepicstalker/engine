local gfx = core.framework.graphics

bbox = {
	ffi.new("GLfloat[24]",
		-0.5, -0.5, -0.5,
		0.5, -0.5, -0.5,
		0.5, 0.5, -0.5,
		-0.5, 0.5, -0.5,
		-0.5, -0.5, 0.5,
		0.5, -0.5, 0.5,
		0.5,  0.5,  0.5,
		-0.5,  0.5,  0.5
	),
	ffi.new("GLuint[16]",
		0, 1, 2, 3,
		4, 5, 6, 7,
		0, 4, 1, 5,
		2, 6, 3, 7
	)				
}
function bbox:draw()
	local vao = gfx.getDefaultVAO()
	GL.glBindVertexArray(vao[0])
	
	local defaultVBO = gfx.getDefaultVBO()
	GL.glBindBuffer(GL.GL_ARRAY_BUFFER,defaultVBO[0])
	GL.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(bbox[1]),bbox[1],GL.GL_STATIC_DRAW)
	
	local defaultEBO = gfx.getDefaultEBO()
	GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER,defaultEBO[0]);
	GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER,ffi.sizeof(bbox[2]),bbox[2],GL.GL_STATIC_DRAW)
	
	local position = gfx.shaders.getAttribLocation("position")
	local normal = gfx.shaders.getAttribLocation("normal")
	local tangent = gfx.shaders.getAttribLocation("tangent")
	local bitangent = 3--gfx.shaders.getAttribLocation("bitangent")
	local texcoord = gfx.shaders.getAttribLocation("texcoord")
	
	GL.glEnableVertexAttribArray(position)
	GL.glDisableVertexAttribArray(normal)
	GL.glDisableVertexAttribArray(tangent)
	GL.glDisableVertexAttribArray(bitangent)
	GL.glDisableVertexAttribArray(texcoord)
	
	GL.glVertexAttribPointer(position,3,GL.GL_FLOAT,0,0,nil)
	
	gfx.transform.updateTransformations()

  	GL.glDrawElements(GL.GL_LINE_LOOP,4,GL.GL_UNSIGNED_INT,nil)
	local pointer = ffi.cast("GLvoid *",4*ffi.sizeof("GLuint"))
	GL.glDrawElements(GL.GL_LINE_LOOP,4,GL.GL_UNSIGNED_INT,pointer)
	pointer = ffi.cast("GLvoid *",8*ffi.sizeof("GLuint"))
	GL.glDrawElements(GL.GL_LINES,8,GL.GL_UNSIGNED_INT,pointer)
	
	GL.glBindVertexArray(0)
end

-- rect_uv
rect_uv = {
	ffi.new("GLfloat[16]",
		-0.5, 0.5, 0, 1, -- Top-left
		0.5, 0.5, 1, 1,  -- Top-right
		0.5, -0.5, 1, 0, -- Bottom-right
		-0.5, -0.5, 0, 0 -- Bottom-left
	),
	ffi.new("GLuint[6]",
		0, 1, 2,
		2, 3, 0
	)
}
function rect_uv:draw(mode,flipH,flipV,scaleX,scaleY)

	local vao = gfx.getDefaultVAO()
	GL.glBindVertexArray(vao[0])
	
	rect_uv[1][2],rect_uv[1][3] = 0,1
	rect_uv[1][6],rect_uv[1][7] = 1,1
	rect_uv[1][10],rect_uv[1][11] = 1,0
	rect_uv[1][14],rect_uv[1][15] = 0,0
		
	if (flipH) then
		rect_uv[1][2] = 1
		rect_uv[1][6] = 0
		rect_uv[1][10] = 0
		rect_uv[1][14] = 1
	end
	if (flipV) then
		rect_uv[1][3] = 0
		rect_uv[1][7] = 0
		rect_uv[1][11] = 1
		rect_uv[1][15] = 1	
	end
	
	local defaultVBO = gfx.getDefaultVBO()
	GL.glBindBuffer(GL.GL_ARRAY_BUFFER,defaultVBO[0])
	GL.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(rect_uv[1]),rect_uv[1],GL.GL_STATIC_DRAW)
	
	local defaultEBO = gfx.getDefaultEBO()
	GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER,defaultEBO[0]);
	GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER,ffi.sizeof(rect_uv[2]),rect_uv[2],GL.GL_STATIC_DRAW)
	
	local position = gfx.shaders.getAttribLocation("position")
	local texcoord = gfx.shaders.getAttribLocation("texcoord")
	
	GL.glEnableVertexAttribArray(position)
	GL.glEnableVertexAttribArray(texcoord)
	
	local stride = 4 * ffi.sizeof("GLfloat")
	local vte = ffi.cast("GLvoid *", 2 * ffi.sizeof("GLfloat"))
	
	GL.glVertexAttribPointer(position,2,GL.GL_FLOAT,0,stride,nil)
	GL.glVertexAttribPointer(texcoord,2,GL.GL_FLOAT,0,stride,vte)

	gfx.transform.updateTransformations()
	GL.glDrawElements(mode or GL.GL_TRIANGLES,6,GL.GL_UNSIGNED_INT,nil)
	
	GL.glBindVertexArray(0)
end