local ASSIMP = require("assimp") ; if not (ASSIMP) then error("Failed to load assimp") end
local _fileIO = nil

local gfx = core.framework.graphics
local transform = core.framework.graphics.transform

cache = {}

--TODO: ASSIMP -- sadly aiImportFileFromMemory cannot open meshes spread across several files, which makes multi-threaded loading difficult to pull off just using aiImportFileEx
-- Will need a dedicated thread with assimp, GL, kazmath loaded

-- core.framework.graphics.import.model(filename,opts)
getmetatable(this).__call = function (self,fname,opts)
	if (cache[ fname ]) then
		return cache[ fname ]
	end
	local model = cModel(fname,opts)
	cache[ fname ] = model
	return model
end

------------------------------------------------------------------------
-- cModel
------------------------------------------------------------------------
Class "cModel"
function cModel:__init(filename,opts)
	self.name = filename
	self.meshes = {}
	self.bbox = {min={0,0,0},max={0,0,0}}
	
	local ext = utils.get_ext(filename)
	if (ext == "ogf") then 
		self:load_ogf(filename,opts)
	else
		self:load_assimp(filename,opts)
	end
	
	self:calculateBoundingBox()
end

-- see: http://assimp.sourceforge.net/lib_html/postprocess_8h.html#a64795260b95f5a4b3f3dc1be4f52e410
local function makeOptions(opts)
	local t = {}
	for i,v in ipairs(opts) do
		if (ffi.C["aiProcess_"..v]) then
			t[#t+1] = ffi.C["aiProcess_"..v]
		end
	end
	return bit.bor(unpack(t))
end

function cModel:load_ogf(filename,opts)
	local node = core.framework.import.binary(filename)
	assert(node:size() > 0)
	local stack = {}
	local function collect_children(node)
		if (node:find_chunk(9) > 0) then
			local cnt = 0
			local chunk = node:open_chunk(9) -- OGF_CHILDREN == 9
			local child = chunk:open_chunk(0)
			while (child and child:size() > 0) do
				table.insert(stack,core.framework.import.binary(nil,nil,nil,child))
				cnt = cnt + 1
				child = chunk:open_chunk(cnt)
			end
		end
	end
	
	-- OGF_S_MOTION_REFS2 = 24
--[[ 	self.motions = {}
	if (node:find_chunk(24) > 0) then
		local skls_cnt = node:r_u32()
 		for x=1,skls_cnt do
			local motion = node:r_stringZ()
			if (motion ~= "") then
				local skls = core.framework.import.binary("models/"..motion:gsub("\\","/")..".skls")
				if (skls and skls:size() > 0) then
					local cnt = skls:r_u32()
					for i=1,cnt do
						print("iteration",i)
						local name = skls:r_stringZ()
						local frameStart = skls:r_u32()
						local frameEnd = skls:r_u32()
						local fps = skls:r_float()
						local vers = skls:r_u16()
						if (vers >= 6) then
							local flags = skls:r_u8()
							local boneOrPart = skls:r_u16()
							local speed = skls:r_float()
							local accrue = skls:r_float()
							local falloff = skls:r_float()
							local power = skls:r_float()
							local boneCount = skls:r_u16()
							for n=1,boneCount do
								local boneName = skls:r_stringZ()
								local motionFlags = skls:r_u8()
								for k=1,6 do
									local behavior1 = skls:r_u8()
									local behavior2 = skls:r_u8()
									local keyCount = skls:r_u16()
									for j=1,keyCount do
										local value = skls:r_float()
										local time = skls:r_float()
										local shape = skls:r_u8()
										if (shape ~= 4) then
											local tension = skls:r_float_q16(-32,32)
											local continuity = skls:r_float_q16(-32,32)
											local bias = skls:r_float_q16(-32,32)
											local param1 = skls:r_float_q16(-32,32)
											local param2 = skls:r_float_q16(-32,32)
											local param3 = skls:r_float_q16(-32,32)
											local param4 = skls:r_float_q16(-32,32)
										end
									end
								end
							end
							if (vers >= 7) then
								-- unimplemented
								skls:r_u32()
								skls:r_string()
								local mcnt = skls:r_u32()
								for j=1,mcnt do
									skls:r_float()
									skls:r_float()
								end
							end
						else
							break
						end
					end
				end
			end
		end
	end ]]

	while (true) do
		if (node) then
			collect_children(node)
			local vertex_chunk = node:open_chunk(3) -- OGF_VERTICES == 3
			if (vertex_chunk and vertex_chunk:size() > 0) then 
				local link_type = vertex_chunk:r_u32()
				local count = vertex_chunk:r_u32()
				if (count > 0) then
					local stride = (3+3+3+3+2)
					local vertices = ffi.new("GLfloat[?]",stride*count)
					for i=0,count-1 do
						if (link_type == 2 or link_type == (2*0x12071980)) then
							vertex_chunk:r_u16()
							vertex_chunk:r_u16()
						elseif (link_type == 3 or link_type == (4 * 0x12071980)) then
							vertex_chunk:r_u16()
							vertex_chunk:r_u16()
							vertex_chunk:r_u16()
						elseif (link_type == 4 or link_type == (5 * 0x12071980)) then
							vertex_chunk:r_u16()
							vertex_chunk:r_u16()
							vertex_chunk:r_u16()
							vertex_chunk:r_u16()
						end
						
						-- Position
						vertices[stride*i+0] = vertex_chunk:r_float()
						vertices[stride*i+1] = vertex_chunk:r_float()
						vertices[stride*i+2] = vertex_chunk:r_float()

						-- Normal
						vertices[stride*i+3] = vertex_chunk:r_float()
						vertices[stride*i+4] = vertex_chunk:r_float()
						vertices[stride*i+5] = vertex_chunk:r_float()
						
						-- Tangent
						vertices[stride*i+6] = vertex_chunk:r_float()
						vertices[stride*i+7] = vertex_chunk:r_float()
						vertices[stride*i+8] = vertex_chunk:r_float()
						
						-- BiTangent
						vertices[stride*i+9] = vertex_chunk:r_float()
						vertices[stride*i+10] = vertex_chunk:r_float()
						vertices[stride*i+11] = vertex_chunk:r_float()
						
						-- Weight
						if (link_type == 2 or link_type == (2*0x12071980)) then
							vertex_chunk:r_float()
						elseif (link_type == 3 or link_type == (4 * 0x12071980)) then
							vertex_chunk:r_float()
							vertex_chunk:r_float()
						elseif (link_type == 4 or link_type == (5 * 0x12071980)) then
							vertex_chunk:r_float()
							vertex_chunk:r_float()
							vertex_chunk:r_float()
						end
						
						-- TexCoord
						vertices[stride*i+12] = vertex_chunk:r_float()
						vertices[stride*i+13] = vertex_chunk:r_float()
						
						if (link_type == 1 or link_type == (1*0x12071980)) then
							vertex_chunk:r_u32()
						end
					end
					
					-- OGF_INDICES == 4
					local indices
					if (node:find_chunk(4) > 0) then
						local count = node:r_u32()
						indices = ffi.new("GLuint[?]",count)
						for i=0,count-1 do 
							indices[i] = node:r_u16()
						end
					end

					local oMesh = cMesh(vertices,indices)
					table.insert(self.meshes, oMesh)
					
					-- OGF_TEXTURE == 2
					if (node:find_chunk(2) > 0) then
						local path = "textures/"..utils.trim(node:r_stringZ():gsub("\\","/"))
						oMesh.textures[ "diffuse" ] = core.framework.graphics.import.image(path..".dds")
						oMesh.shader = "default3D"
						
						oMesh.material = core.framework.graphics.import.material()
						oMesh.material:diffuseMap(path..".dds")
					end
				end
			end
		end 
		if (#stack == 0) then
			break
		end
		node = stack[#stack]
		stack[#stack] = nil
	end
end

local assimpTextureTypes = {
	["diffuse"]          = ffi.C.aiTextureType_DIFFUSE,
	["specular"]         = ffi.C.aiTextureType_SPECULAR,
	["ambient"]           = ffi.C.aiTextureType_AMBIENT,
	["emissive"]          = ffi.C.aiTextureType_EMISSIVE,
	["height"]            = ffi.C.aiTextureType_HEIGHT,
	["normals"]           = ffi.C.aiTextureType_NORMALS,
	["shininess"]         = ffi.C.aiTextureType_SHININESS,
	["opacity"]           = ffi.C.aiTextureType_OPACITY,
	["displacement"]      = ffi.C.aiTextureType_DISPLACEMENT,
	["lightmap"]          = ffi.C.aiTextureType_LIGHTMAP,
	["reflection"]        = ffi.C.aiTextureType_REFLECTION,
	["metallicRoughness"] = ffi.C.aiTextureType_UNKNOWN
}

local textureUnits = {
--	["environmentEnvSampler"] 	= GL.GL_TEXTURE0,
--	["diffuseEnvSampler"]     	= GL.GL_TEXTURE1,
--	["specularEnvSampler"]		= GL.GL_TEXTURE2,
	["diffuse"]               	= GL.GL_TEXTURE3,
	["metallicRoughness"]     	= GL.GL_TEXTURE4,
	["normals"]               	= GL.GL_TEXTURE5,
--	["brdfLUT"]               	= GL.GL_TEXTURE6,
	["emissive"]             	= GL.GL_TEXTURE7,
	["lightmap"]              	= GL.GL_TEXTURE8
}

local textureUnit = {
	["diffuseMap"] 			= GL.GL_TEXTURE3,
	["roughnessMap"] 		= GL.GL_TEXTURE4,
	["normalMap"] 			= GL.GL_TEXTURE5,
	["emissiveMap"] 		= GL.GL_TEXTURE7,
}

function cModel:load_assimp(filename,opts)
	jit.off() -- C callback to Lua, compilation not allowed
	self.scene = ASSIMP.aiImportFileEx(filename,makeOptions(opts),getPHYSFSFileIO())
	jit.on()

	if (self.scene == nil) then
		local err = ASSIMP.aiGetErrorString()
		error( "ASSIMP aiImportFileEx failure: " .. (err == nil and "unknown" or ffi.string(err)) ,3 )
	end
	
	ffi.gc(self.scene,function(scene) ASSIMP.aiReleaseImport(scene) end)
	local num_materials = self.scene.mNumMaterials
	
	local node = self.scene.mRootNode
	local stack,size = {},0

	while true do
		if (node) then
			for i=0, node.mNumMeshes-1 do
				-- meshes
				local mesh = self.scene.mMeshes[ node.mMeshes[i] ]
				if (mesh.mNumVertices > 0) then
					local HAS_NORMALS = false
					local HAS_TANGENTS = false
					local HAS_UV = false
					
					local stride = (3+3+3+3+2)
					local vertices = ffi.new("GLfloat[?]",stride*mesh.mNumVertices)
					for i=0, mesh.mNumVertices-1 do
						vertices[stride*i] = mesh.mVertices[i].x
						vertices[stride*i+1] = mesh.mVertices[i].y
						vertices[stride*i+2] = mesh.mVertices[i].z

						if (mesh.mNormals ~= nil) then
							HAS_NORMALS = true
							vertices[stride*i+3] = mesh.mNormals[i].x
							vertices[stride*i+4] = mesh.mNormals[i].y
							vertices[stride*i+5] = mesh.mNormals[i].z
						end

						if (mesh.mTangents ~= nil) then
							HAS_TANGENTS = true
							vertices[stride*i+6] = mesh.mTangents[i].x
							vertices[stride*i+7] = mesh.mTangents[i].y
							vertices[stride*i+8] = mesh.mTangents[i].z
							
							vertices[stride*i+9] = mesh.mBitangents[i].x
							vertices[stride*i+10] = mesh.mBitangents[i].y
							vertices[stride*i+11] = mesh.mBitangents[i].z
						end
			
						if (mesh.mTextureCoords ~= nil and mesh.mTextureCoords[0] ~= nil) then
							HAS_UV = true
							vertices[stride*i+12] = mesh.mTextureCoords[0][i].x
							vertices[stride*i+13] = mesh.mTextureCoords[0][i].y
						end
					end

					local indices = ffi.new("GLuint[?]",mesh.mNumFaces*3)
					for i=0, mesh.mNumFaces-1 do
						local face = mesh.mFaces[i]
						indices[3*i] = face.mIndices[0]
						indices[3*i+1] = face.mIndices[1]
						indices[3*i+2] = face.mIndices[2]
					end

					local oMesh = cMesh(vertices,indices)
					table.insert(self.meshes, oMesh)
			
					-- materials
					if (num_materials > 0) then
						local mtl = self.scene.mMaterials[mesh.mMaterialIndex]
						local aiString = ffi.new("struct aiString *",ffi.C.malloc(ffi.sizeof("struct aiString")))
						local texMapping = ffi.new("enum aiTextureMapping *",ffi.C.malloc(ffi.sizeof("enum aiTextureMapping")))
						local texOp = ffi.new("enum aiTextureOp *",ffi.C.malloc(ffi.sizeof("enum aiTextureOp")))
						local texMapMode = ffi.new("enum aiTextureMapMode *",ffi.C.malloc(ffi.sizeof("enum aiTextureMapMode")))
						local uvindex = ffi.new("unsigned int*")
						local blend = ffi.new("float*")
						local flags = ffi.new("unsigned int*")
						
						local USE_IBL = false
						local HAS_DIFFUSE = false
						local HAS_EMISSIVEMAP = false
						local HAS_NORMALMAP = false
						local HAS_METALROUGHNESSMAP = false
						local HAS_OCCLUSIONMAP = false
						
						oMesh.material = core.framework.graphics.import.material()
						
						for k,typ in pairs(assimpTextureTypes) do
							local count = ASSIMP.aiGetMaterialTextureCount(mtl,typ)
							if (count > 0) then
								if (ASSIMP.aiGetMaterialTexture(mtl,typ,0,aiString,texMapping,uvindex,blend,texOp,texMapMode,flags) == ffi.C.aiReturn_SUCCESS) then
									local path = ffi.string(aiString.data,aiString.length)
									oMesh.textures[ k ] = core.framework.graphics.import.image("textures/"..path)
									if (k == "diffuse") then
										HAS_DIFFUSE = true
										oMesh.material:diffuseMap("textures/"..path)
									elseif (k == "emissive") then
										HAS_EMISSIVEMAP = true
										oMesh.material:emissiveMap("textures/"..path)
									elseif (k == "normals") then
										HAS_NORMALMAP = true
										oMesh.material:normalMap("textures/"..path)
									elseif (k == "metallicRoughness") then
										HAS_METALROUGHNESSMAP = true
										oMesh.material:roughnessMap("textures/"..path)
									elseif (k == "specular") then
										oMesh.material:specularMap("textures/"..path)
									end
								end
							end
						end
						
						oMesh.vertexShaderOptions = {HAS_NORMALS=HAS_NORMALS,HAS_TANGENTS=HAS_TANGENTS,HAS_UV=HAS_UV}
						oMesh.fragmentShaderOptions = {HAS_DIFFUSE=HAS_DIFFUSE,HAS_EMISSIVEMAP=HAS_EMISSIVEMAP,HAS_NORMALMAP=HAS_NORMALMAP}
						oMesh.shader = "GlTFPBR"
					end
		
					-- bones
					print("mesh.mNumBones",mesh.mNumBones)
					oMesh.bones = { count=mesh.mNumBones }
					for i=0, mesh.mNumBones-1 do
						local boneName = ffi.string(mesh.mBones[i].mName.data)
						local boneData = mesh.mBones[i]
						
						oMesh.bones[ i+1 ] = { name=boneName, weights={}, offset=ffi.cast("kmMat4 &",boneData.mOffsetMatrix[0]) }
						
						for j=0, boneData.mNumWeights-1 do
							oMesh.bones[ i+1 ].weights[ j ] = { vertexID=boneData.mWeights[ j ].mVertexId, weight=boneData.mWeights[j].mWeight }
						end
					end
				end
			end
			for i=node.mNumChildren-1,0,-1 do
				local child = node.mChildren[i]
				size = size + 1
				stack[size] = child
			end
		end
		if (size == 0) then
			break
		end
		node = stack[size]
		stack[size] = nil
		size = size - 1
	end
end

function cModel:applyMaterial(mtl)

end

function cModel:draw(reskin,shaders)
	local mode = transform.getMatrixMode()
	transform.setMatrixMode("model")

	for i, mesh in ipairs(self.meshes) do
		transform.push()
		
		mesh:draw(reskin and reskin[i],shaders and shaders[i])
		
		transform.pop()
	end
	
	transform.setMatrixMode(mode)
end

function cModel:drawBBox(color)
	gfx.shaders.useProgram("default3D")
	transform.push()

		transform.translate((self.bbox.min[1]+self.bbox.max[1])/2,(self.bbox.min[2]+self.bbox.max[2])/2,(self.bbox.min[3]+self.bbox.max[3])/2)
		transform.scale(self.bbox.max[1]-self.bbox.min[1],self.bbox.max[2]-self.bbox.min[2],self.bbox.max[3]-self.bbox.min[3])
		
		gfx.setColor(unpack(color))
		GL.glActiveTexture(GL.GL_TEXTURE0)
		
		gfx.primitive.bbox:draw()
		
		gfx.setColor(unpack(gfx.color.White))
	
	gfx.transform.pop()
end

function cModel:calculateBoundingBox()
	for i,o in ipairs(self.meshes) do
		o:calculateBoundingBox()
		self.bbox.min[1] = math.min(self.bbox.min[1],o.bbox.min[1])
		self.bbox.min[2] = math.min(self.bbox.min[2],o.bbox.min[2])
		self.bbox.min[3] = math.min(self.bbox.min[3],o.bbox.min[3])

		self.bbox.max[1] = math.max(self.bbox.max[1],o.bbox.max[1])
		self.bbox.max[2] = math.max(self.bbox.max[2],o.bbox.max[2])
		self.bbox.max[3] = math.max(self.bbox.max[3],o.bbox.max[3])
	end
end

function cModel:__gc()

end

------------------------------------------------------------------------
-- cMesh
------------------------------------------------------------------------
Class "cMesh"
function cMesh:__init(vertices,indices)
	self.vertices = vertices
	self.vertices_size = ffi.sizeof(vertices) / ffi.sizeof("GLfloat")
	
	self.indices = indices
	self.indices_size = ffi.sizeof(indices) / ffi.sizeof("GLuint")
		
	self.bbox = {min={0,0,0},max={0,0,0}}
	self.textures = {}
	self.material = nil
	
	self.vao = ffi.new("GLuint[1]")
	self.vbo = ffi.new("GLuint[1]")
	self.ebo = ffi.new("GLuint[1]")
	
	ffi.gc(self.vao,function(buffer) GL.glDeleteVertexArrays(1, buffer) end)
	ffi.gc(self.vbo,function(buffer) GL.glDeleteBuffers(1,buffer) end)
	ffi.gc(self.ebo,function(buffer) GL.glDeleteBuffers(1,buffer) end)
	
	GL.glGenVertexArrays(1,self.vao)
	GL.glGenBuffers(1,self.vbo)
	GL.glGenBuffers(1,self.ebo)
	
	gfx.shaders.useProgram("default3D")
	GL.glBindVertexArray(self.vao[0])
	
	GL.glBindBuffer(GL.GL_ARRAY_BUFFER,self.vbo[0])
	GL.glBufferData(GL.GL_ARRAY_BUFFER,ffi.sizeof(vertices),vertices,GL.GL_STATIC_DRAW)
	
	GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER,self.ebo[0])
	GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER,ffi.sizeof(indices),indices,GL.GL_STATIC_DRAW)
		
	local position = gfx.shaders.getAttribLocation("position")
	local normal = gfx.shaders.getAttribLocation("normal")
	local tangent = gfx.shaders.getAttribLocation("tangent")
	local bitangent = 3--gfx.shaders.getAttribLocation("bitangent") -- FIX ME
	local texcoord = gfx.shaders.getAttribLocation("texcoord")

	GL.glEnableVertexAttribArray(position)
	GL.glEnableVertexAttribArray(normal)
	GL.glEnableVertexAttribArray(tangent)
	GL.glEnableVertexAttribArray(bitangent)
	GL.glEnableVertexAttribArray(texcoord)
	
	local stride = (3+3+3+3+2)*ffi.sizeof("GLfloat")
	local vno = ffi.cast("GLvoid *",3*ffi.sizeof("GLfloat"))  -- hold in local variable because of GC
	local vta = ffi.cast("GLvoid *",(3+3)*ffi.sizeof("GLfloat"))
	local vbi = ffi.cast("GLvoid *",(3+3+3)*ffi.sizeof("GLfloat"))
	local vte = ffi.cast("GLvoid *",(3+3+3+3)*ffi.sizeof("GLfloat"))
	
	GL.glVertexAttribPointer(position,3,GL.GL_FLOAT,0,stride,nil)
	GL.glVertexAttribPointer(normal,3,GL.GL_FLOAT,0,stride,vno)
	GL.glVertexAttribPointer(tangent,3,GL.GL_FLOAT,0,stride,vta)
	GL.glVertexAttribPointer(bitangent,3,GL.GL_FLOAT,0,stride,vbi)
	GL.glVertexAttribPointer(texcoord,2,GL.GL_FLOAT,0,stride,vte)
	
	GL.glBindVertexArray(0)
end

function cMesh:draw(reskin,shader)
	gfx.shaders.useProgram(shader or self.shader or "default3D")
	
	GL.glBindVertexArray(self.vao[0])

	GL.glActiveTexture(GL.GL_TEXTURE3)
	GL.glBindTexture(GL.GL_TEXTURE_2D,gfx.getDefaultTexture()[0])
		
	for name, typ in pairs(textureUnits) do
		local image = reskin and reskin[ name ] or self.textures[ name ]
		if (image and image.loaded) then
			GL.glActiveTexture(typ)
			GL.glBindTexture(GL.GL_TEXTURE_2D,image.texture[0])	
		end
	end
	
	-- TEST CODE
	-- TODO: COMPLETE ME
	-- if (self.material) then
		-- local prog = gfx.shaders.getActive()
		
		-- GL.glLineWidth(self.material:lineWidth())
		-- local index = GL.glGetUniformLocation(prog,"u_pointSize")
		-- local pointSize = ffi.new("GLfloat[1]",self.material:pointSize())
		-- GL.glUniform1fv(index,1,pointSize)
		
		
		-- if (self.material:wireframe()) then
			-- GL.glPolygonMode(GL.GL_FRONT_AND_BACK,GL.GL_LINE)
		-- else
			-- GL.glPolygonMode(GL.GL_FRONT_AND_BACK,GL.GL_FILL)
		-- end
		
		-- for name, typ in pairs(textureUnit) do
			-- local image = self.material[ name ](self.material)
			-- if (image and image.loaded) then
				-- GL.glActiveTexture(typ)
				-- GL.glBindTexture(GL.GL_TEXTURE_2D,image.texture[0])	
			-- end
		-- end
	-- end
	
	gfx.transform.updateTransformations()
	gfx.drawElements(GL.GL_TRIANGLES,self.indices_size)
	
	GL.glBindVertexArray(0)
	GL.glActiveTexture(GL.GL_TEXTURE0)
end

function cMesh:drawBBox()
	gfx.shaders.useProgram("default3D")
	transform.push()
	
		transform.translate((self.bbox.min[1]+self.bbox.max[1])/2,(self.bbox.min[2]+self.bbox.max[2])/2,(self.bbox.min[3]+self.bbox.max[3])/2)
		transform.scale(self.bbox.max[1]-self.bbox.min[1],self.bbox.max[2]-self.bbox.min[2],self.bbox.max[3]-self.bbox.min[3])
		
		gfx.setColor(unpack(gfx.color.Red))
		
		gfx.primitive.bbox:draw()
		
		gfx.setColor(unpack(gfx.color.White))
	
	gfx.transform.pop()
end

function cMesh:calculateBoundingBox()
	local stride = (3+3+3+3+2)
	local sz = ffi.sizeof(self.vertices)/stride-1
	for i=0,sz,stride do
		local x = self.vertices[i]
		local y = self.vertices[i+1]
		local z = self.vertices[i+2]
		
		if (i == 0) then
			self.bbox.min[1],self.bbox.max[1] = x,x
			self.bbox.min[2],self.bbox.max[2] = y,y
			self.bbox.min[3],self.bbox.max[3] = z,z
		else
			self.bbox.min[1] = math.min(self.bbox.min[1], x)
			self.bbox.min[2] = math.min(self.bbox.min[2], y)
			self.bbox.min[3] = math.min(self.bbox.min[3], z)

			self.bbox.max[1] = math.max(self.bbox.max[1], x)
			self.bbox.max[2] = math.max(self.bbox.max[2], y)
			self.bbox.max[3] = math.max(self.bbox.max[3], z)
		end
	end
end

function cMesh:__gc()

end

------------------------
function getPHYSFSFileIO()
 	if ( _fileIO ) then
		return _fileIO
	end

	local function PHYSFSReadProc( self, buffer, size, count )
		local file = ffi.cast( "void *", self.UserData )
		return physfs.PHYSFS_read( file, buffer, size, count )
	end
	local function PHYSFSWriteProc( self, buffer, size, count )
		local file = ffi.cast( "void *", self.UserData )
		return physfs.PHYSFS_write( file, buffer, size, count )
	end
	local function PHYSFSTellProc( self )
		local file = ffi.cast( "void *", self.UserData )
		return physfs.PHYSFS_tell( file )
	end
	local function PHYSFSFileSizeProc( self )
		local file = ffi.cast( "void *", self.UserData )
		return physfs.PHYSFS_fileLength( file )
	end
	local function PHYSFSSeekProc( self, offset, whence )
		local file = ffi.cast( "void *", self.UserData )
		local ret  = physfs.PHYSFS_seek( file, offset )
		if ( ret ~= 0 ) then
			return ffi.C.aiReturn_SUCCESS
		else
			-- TODO: Return ffi.C.aiReturn_OUTOFMEMORY.
			return ffi.C.aiReturn_FAILURE
		end
	end
	local function PHYSFSFlushProc( self )
		local file = ffi.cast( "void *", self.UserData )
		physfs.PHYSFS_flush( file )
	end
		
	local function PHYSFSOpenProc( self, filename, mode )
		local file        = ffi.new("struct aiFile")
		file.ReadProc     = PHYSFSReadProc
		file.WriteProc    = PHYSFSWriteProc
		file.TellProc     = PHYSFSTellProc
		file.FileSizeProc = PHYSFSFileSizeProc
		file.SeekProc     = PHYSFSSeekProc
		file.FlushProc    = PHYSFSFlushProc
		local handle      = physfs.PHYSFS_openRead( filename )
		file.UserData     = ffi.cast( "void *", handle )
		return handle ~= nil and file or nil
	end

	local function PHYSFSCloseProc( self, file )
		file.ReadProc:free()
		file.WriteProc:free()
		file.TellProc:free()
		file.FileSizeProc:free()
		file.SeekProc:free()
		file.FlushProc:free()
		local userdata = ffi.cast( "void *", file.UserData )
		physfs.PHYSFS_close( userdata )
	end
	
	local fileIO = ffi.new("struct aiFileIO")
	fileIO.OpenProc = PHYSFSOpenProc
	fileIO.CloseProc = PHYSFSCloseProc
	_fileIO = fileIO
	return fileIO
end