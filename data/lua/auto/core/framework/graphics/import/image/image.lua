cache = {}

skip_thread_pool = false 			-- Temporary flag that should be used to load image mandatorily

-- core.framework.graphics.import.image(filename,opts,callback)
getmetatable(this).__call = function (self,fname,opts,callback)
	if (cache[fname]) then
		return cache[fname]
	end 
	if not (filesystem.exists(fname) and not filesystem.isDirectory(fname)) then
		print("[Warning] file does not exist:",fname)
		return
	end
	local image = cImage(fname,opts,callback)
	cache[fname] = image
	return image
end

function exists(fname)
	return true --cache[fname] ~= nil or (filesystem.exists(fname) and not filesystem.isDirectory(fname))
end

function on_game_start()
	CallbackSet("framework_load",framework_load)
end 

function framework_load()
	-- Autoload all texture atlases
	local function on_execute(path,fname,fullpath)
		local xml = core.framework.import.xml():parse(fullpath)
		for i,element in ipairs(xml.data.xml[ 1 ].atlas) do
			if (element.texture) then
				skip_thread_pool = true -- immediate
				local fname = element[ 1 ].file
				local alias = element[ 1 ].alias or fname
				local src_image = cache[ fname ] or cImage(fname)
				cache[ fname ] = src_image
				cache[ alias ] = src_image
				
				-- subimage from src
				for j,texture in ipairs(element.texture) do
					local node = texture[ 1 ]
					if (cache[ node.name ]) then
						print("[Warning] Texture alias redifinition!",fname,node.name)
					else
						local options = {}
						options.flipH = node.flipH == "true"
						options.flipV = node.flipV == "true"
						options.mipmaps = node.mipmaps == "true"
						
						if (texture.gl) then
							for j,gl in ipairs(texture.gl) do
								if (gl[1].var and gl[2]) then
									table.insert(options.GL,GL[ gl[1].var ],GL[ gl[2] ])
								end
							end 
						end 
					
						local subimage = cImage(fname,options)
						subimage:subImage(texture[1].name,src_image,tonumber(texture[ 1 ].x),tonumber(texture[ 1 ].y),tonumber(texture[ 1 ].w),tonumber(texture[ 1 ].h))
						cache[ texture[1].name ] = subimage
					end
				end
			-- Subimage from given tile width and height
			elseif (element[ 1 ].width and element[ 1 ].height) then
				skip_thread_pool = true -- immediate
				local fname = element[ 1 ].file
				local alias = element[ 1 ].alias or fname
				
				local src_image = cache[ fname ] or cImage(fname)
				cache[ fname ] = src_image
				cache[ alias ] = src_image
				
				local width = tonumber(element[ 1 ].width)
				local height = tonumber(element[ 1 ].height)
				
				local x,y,id = 0,0,1
				for i=1, src_image.height/height do
					x = 0
					for j=1, src_image.width/width do
						local subimage = cImage(element[ 1 ].file)
						local name = alias .. "." .. id -- Autogenerate alias name for each tile to be *.[index] (ex. tileset1.0, tileset1.20, etc.)
						subimage:subImage(name,src_image,x,y,width,height)
						cache[ name ] = subimage
						x = x + width
						id = id + 1
					end
					y = y + height
				end
			end
		end
	end
	filesystem.fileForEach("configs/texture/atlas",true,{"xml"},on_execute)
end

function exists(alias)
	return cache[alias] ~= nil
end

------------------------------------------------------------------------
-- helpers
------------------------------------------------------------------------
function reinit(filename,buffer,length)
	local image = core.framework.graphics.import.image.cache[filename]
	if (image) then
		image:reinit(filename,buffer,length)
	else 
		print("no image cache for",filename)
	end
end

function openImage(filename)
	local file = physfs.PHYSFS_openRead(filename)
	if (file ~= nil) then
		local length = physfs.PHYSFS_fileLength(file)
		local buffer = ffi.new("char[?]",length)
		physfs.PHYSFS_read(file,buffer,1,length)
		physfs.PHYSFS_close(file)
		local data = ffi.string(buffer,length)
		linda:send("main-call",{"core.framework.graphics.import.image.reinit",{filename,data,tonumber(length)}})
	else 
		print("cImage:__init: " .. ffi.string(physfs.PHYSFS_getLastError()) .. " - " .. filename)
	end
end
------------------------------------------------------------------------
-- cImage
------------------------------------------------------------------------
Class "cImage"
function cImage:__init(filename,opts,callback)
	self.name = filename
	self.opts = opts
	self.callback = callback -- on reinited when image is loaded
	if not (cache[filename]) then
		if (#core.framework.thread.pool() == 0 or skip_thread_pool == true) then
			local buffer,length = filesystem.read(filename)
			assert(length and length > 0,strformat("failed to load %s",filename))
			self:reinit(filename,buffer,length)
			skip_thread_pool = false
			return
		end
		-- read file in a separate thread using our thread pool
		core.framework.thread.pool.linda:send("pool-functor",{openImage,{filename}})
	end
end

function cImage:reinit(filename,buffer,length)
	self.image = ffi.new("ILuint[1]")
	IL.ilGenImages(1, self.image)
	IL.ilBindImage(self.image[0])

	IL.ilLoadL(IL.IL_TYPE_UNKNOWN, buffer, length)
	--IL.ilConvertImage(IL.IL_RGBA, IL.IL_UNSIGNED_BYTE)
	local err = IL.ilGetError()
	if (err ~= 0) then
		error(IL.iluErrorString(err))
	end
	self.image_format = IL.ilGetInteger(IL.IL_IMAGE_FORMAT)
	self.image_type = IL.ilGetInteger(IL.IL_IMAGE_TYPE)
	self.miplevels = IL.ilGetInteger(IL.IL_NUM_MIPMAPS)
	self.src_width  = IL.ilGetInteger(IL.IL_IMAGE_WIDTH)
	self.src_height = IL.ilGetInteger(IL.IL_IMAGE_HEIGHT)
	self.src_pixels = IL.ilGetData()

	self.x = 0
	self.y = 0
	self.width = self.src_width
	self.height = self.src_height
	
	self.texture = ffi.new("GLuint[1]")
	GL.glGenTextures(1, self.texture)
	GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture[0])
	local aniso = ffi.new("GLfloat[1]")
	GL.glGetFloatv(GL.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, aniso)	
	GL.glTexParameterf(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MAX_ANISOTROPY_EXT, aniso[0])
	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_WRAP_S,GL.GL_REPEAT)
	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_WRAP_T,GL.GL_REPEAT)
	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_BASE_LEVEL,0)
	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MAX_LEVEL,self.miplevels)
	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MIN_LOD,0)
	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MAX_LOD,self.miplevels)

	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MIN_FILTER,GL.GL_LINEAR)
	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MAG_FILTER,GL.GL_LINEAR)

	if (self.opts and self.opts.GL) then
		for i,t in ipairs(self.opts.GL) do
			GL.glTexParameteri(GL.GL_TEXTURE_2D,t[1],t[2])
		end
	end

	self.pixels = self.src_pixels
	GL.glTexImage2D(GL.GL_TEXTURE_2D,0,self.image_format,self.src_width,self.src_height,0,self.image_format,self.image_type,self.pixels)

	if (self.opts and self.opts.mipmaps) then
		GL.glGenerateMipmap(self.texture[0])
	end

	self.loaded = true
	
	if (self.callback) then
		self.callback(filename,self.image)
	end
end

function cImage:subImage(alias,src,x,y,w,h)
	self.name = alias
	self.x = x or 0
	self.y = y or 0
	self.width = w or 0
	self.height = h or 0
	self.src_width  = src.src_width
	self.src_height = src.src_height
	self.src_pixels = src.src_pixels
	
	self.texture = ffi.new("GLuint[1]")
	GL.glGenTextures(1, self.texture)
	GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture[0])
	
 	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MIN_FILTER,GL.GL_LINEAR)
	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MAG_FILTER,GL.GL_LINEAR)
	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_BASE_LEVEL,0)
	GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MAX_LEVEL,0)

	self.pixels = self.src_pixels + (self.x+self.y*src.width)*4
	
	if (self.opts) then
		if (self.opts.GL) then
			for i,t in ipairs(self.opts.GL) do
				GL.glTexParameteri(GL.GL_TEXTURE_2D,t[1],t[2])
			end
		end
	end

	GL.glPixelStorei(GL.GL_UNPACK_ROW_LENGTH, self.src_width )
	GL.glTexImage2D( GL.GL_TEXTURE_2D, 0, src.image_format, self.width, self.height, 0, src.image_format, src.image_type, self.pixels )
	GL.glPixelStorei( GL.GL_UNPACK_ROW_LENGTH, 0 )

	if (self.opts and self.opts.mipmaps) then
		GL.glGenerateMipmap(self.texture[0])
	end
	
	self.loaded = true
end

function cImage:draw(x, y, r, sx, sy, ox, oy, kx, ky)
	if not (self.loaded) then 
		return
	end 
	local gfx = core.framework.graphics
	
	local vao = gfx.getDefaultVAO()
	GL.glBindVertexArray(vao[0])
	
	local mode = gfx.transform.getMatrixMode()
	gfx.transform.setMatrixMode("model")
	gfx.transform.push()

		sx = sx or 1
		sy = sy or 1
		if (x or y) then
			gfx.transform.translate(x or 0,y or 0,0)
		end
		if (r) then
			gfx.transform.rotateZ(r)
		end
		gfx.transform.scale(self.width*sx,self.height*sy)
		
		GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture[0])

		local fh, fv
		if (self.opts) then
			fh, fv = self.opts.flipH, self.opts.flipV
		end	
		gfx.primitive.rect_uv:draw(nil,fh,fv)
		
	gfx.transform.pop()
	gfx.transform.setMatrixMode(mode)
	
	GL.glBindVertexArray(0)
end

function cImage:getTexture()
	return self.texture or gfx.getDefaultTexture()
end

function cImage:getRegion()
	return (self.x or 0),(self.y or 0),(self.width or 0),(self.height or 0)
end

function cImage:getTextureSize()
	return (self.width or 0),(self.height or 0)
end

function cImage:__gc()
	if (self.texture ~= nil) then
		GL.glDeleteTextures(1,self.texture)
	end
	if (self.image ~= nil) then
		IL.ilDeleteImages(1,self.image)
	end
end