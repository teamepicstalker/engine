local FT = require("freetype")	; if not (FT) then error("Failed to load freetype") end

cache = {}

-- TODO: Piggyback off of Nuklear font baking/atlas

-- core.framework.graphics.import.font(filename)
getmetatable(this).__call = function (self,fname,size)
	size = tonumber(size)
	if (cache[fname] and cache[fname][size]) then 
		return cache[fname][size]
	end
	if not (cache[fname]) then 
		cache[fname] = {}
	end

	local fnt = cFont(fname, size)
	cache[fname][size] = fnt
	return fnt
end
------------------------------------------------------------------------
-- cFont
------------------------------------------------------------------------
Class "cFont"
function cFont:__init(filename, size)
	size = size or 16

	local ft = ffi.new("FT_Library[1]")
	FT.FT_Init_FreeType(ft)

	self.face = ffi.new("FT_Face[1]")
	self.buffer, self.length = filesystem.read(filename)
	FT.FT_New_Memory_Face(ft[0], self.buffer, self.length, 0, self.face)

	self.size = size
	size = size * core.framework.window.getPixelScale()
	FT.FT_Set_Pixel_Sizes(self.face[0], 0, size)

	self.texture = ffi.new("GLuint[1]")
	GL.glGenTextures(1, self.texture)
	GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture[0])

	GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_BASE_LEVEL, 0)
	GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
	local o = GL.GL_ONE
	local r = GL.GL_RED
	local mask = ffi.new("GLint[4]",{o,o,o,r})
	GL.glTexParameteriv(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_SWIZZLE_RGBA, mask)
	
	self.pVertices = ffi.new("GLfloat[?]", 24)
end

function cFont:text(txt, x, y, r, sx, sy, ox, oy, kx, ky)
	local gfx 		 = core.framework.graphics
	
	local vao = gfx.getDefaultVAO()
	GL.glBindVertexArray(vao[0])
	
	local defaultVBO = gfx.getDefaultVBO()
	
	local position   = gfx.shaders.getAttribLocation("position")
	local texcoord   = gfx.shaders.getAttribLocation("texcoord")

	GL.glEnableVertexAttribArray(position)
	GL.glEnableVertexAttribArray(texcoord)
	
	local stride     = 4 * ffi.sizeof("GLfloat")
	local pointer    = ffi.cast("GLvoid *", 2 * ffi.sizeof("GLfloat"))
	GL.glBindBuffer(GL.GL_ARRAY_BUFFER, defaultVBO[0])
	GL.glVertexAttribPointer(position, 2, GL.GL_FLOAT, 0, stride, nil)
	GL.glVertexAttribPointer(texcoord, 2, GL.GL_FLOAT, 0, stride, pointer)
	gfx.transform.updateTransformations()
	GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture[0])
	GL.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, 1)

	GL.glDisable(GL.GL_DEPTH_TEST)

	local face = self.face[0]
	for i = 1, #txt do
		local char = string.sub(txt, i, i)
		if (FT.FT_Load_Char(face, string.byte(char), 4) == 0) then
			local g        = face.glyph
			local gx       = x + g.bitmap_left
			local gy       = y + face.size.metrics.ascender / 64 - g.bitmap_top
			local w    = g.bitmap.width
			local h   = g.bitmap.rows
			local v = self.pVertices
			v[0],v[1],v[2],v[3] 	= gx,	gy+h,	0,	1
			v[4],v[5],v[6],v[7]		= gx+w,	gy+h,	1,	1
			v[8],v[9],v[10],v[11] 	= gx,	gy,		0,	0
			v[12],v[13],v[14],v[15] = gx+w,	gy+h,	1,	1
			v[16],v[17],v[18],v[19] = gx+w,	gy,		1,	0
			v[20],v[21],v[22],v[23] = gx, 	gy,		0,	0
			local size = ffi.sizeof(v)
			GL.glBufferData(
				GL.GL_ARRAY_BUFFER,
				size,
				v,
				GL.GL_STREAM_DRAW
			)
			GL.glTexImage2D(
				GL.GL_TEXTURE_2D,
				0,
				GL.GL_RED,
				g.bitmap.width,
				g.bitmap.rows,
				0,
				GL.GL_RED,
				GL.GL_UNSIGNED_BYTE,
				g.bitmap.buffer
			)
			gfx.drawArrays(GL.GL_TRIANGLES, 0, 6) -- 24/4
			x = x + (g.advance.x / 64)
			y = y + (g.advance.y / 64)
		else
			error("Could not load character '" .. char .. "'", 3)
		end
	end
	GL.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, 4)
	
	GL.glBindVertexArray(0)
	GL.glEnable(GL.GL_DEPTH_TEST)
	
--[[ 	local len = #txt
	local text_len = 0
	local str = ffi.new("const char[1]",txt)
	local unicode = ffi.new("nk_rune[1]")
	local nxt = ffi.new("nk_rune[1]")
	local glyph_len = 0
	local next_glyph_len = 0
	local font_glyph = ffi.new("struct nk_user_font_glyph[1]")
	
	glyph_len = nuklear.nk_utf_decode(str,unicode,len)
	if (glyph_len == 0) then
		return
	end
	
	while (text_len < len and glyph_len > 0) then
	    next_glyph_len = nuklear.nk_utf_decode(str[0] + text_len + glyph_len, nxt, len - text_len)
        font.handle:query(font.handle.userdata,font_height, font_glyph, unicode,(nxt == nuklear.NK_UTF_INVALID) and '\0' or nxt)
	end ]]
	
	
	--[[
	    float x = 0;
    int text_len = 0;
    nk_rune unicode = 0;
    nk_rune next = 0;
    int glyph_len = 0;
    int next_glyph_len = 0;
    struct nk_user_font_glyph g;

    NK_ASSERT(list);
    if (!list || !len || !text) return;
    if (!NK_INTERSECT(rect.x, rect.y, rect.w, rect.h,
        list->clip_rect.x, list->clip_rect.y, list->clip_rect.w, list->clip_rect.h)) return;

    nk_draw_list_push_image(list, font->texture);
    x = rect.x;
    glyph_len = nk_utf_decode(text, &unicode, len);
    if (!glyph_len) return;

    /* draw every glyph image */
    fg.a = (nk_byte)((float)fg.a * list->config.global_alpha);
    while (text_len < len && glyph_len) {
        float gx, gy, gh, gw;
        float char_width = 0;
        if (unicode == NK_UTF_INVALID) break;

        /* query currently drawn glyph information */
        next_glyph_len = nk_utf_decode(text + text_len + glyph_len, &next, (int)len - text_len);
        font->query(font->userdata, font_height, &g, unicode,
                    (next == NK_UTF_INVALID) ? '\0' : next);

        /* calculate and draw glyph drawing rectangle and image */
        gx = x + g.offset.x;
        gy = rect.y + g.offset.y;
        gw = g.width; gh = g.height;
        char_width = g.xadvance;
        nk_draw_list_push_rect_uv(list, nk_vec2(gx,gy), nk_vec2(gx + gw, gy+ gh),
            g.uv[0], g.uv[1], fg);

        /* offset next glyph */
        text_len += glyph_len;
        x += char_width;
        glyph_len = next_glyph_len;
        unicode = next;
    }
	--]]
end

function cFont:__gc()
	GL.glDeleteTextures(1, self.texture)
	FT.FT_Done_Face(self.face)
end