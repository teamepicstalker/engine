-- core.framework.graphics.import.material(filename)
getmetatable(this).__call = function (self,fname)
	return cMaterial(fname)
end

Class "cMaterial"
function cMaterial:__init(fname)
	self.properties = {
		["ambient"] 					= {1.0,1.0,1.0,1.0},		-- range [0,1] 		: The RGB components of the ambient light reflected by the material.
		["diffuse"] 					= {1.0,1.0,1.0,1.0},		-- range [0,1] 		: The RGB components of the diffuse light reflected by the material.
		["specular"] 					= {1.0,1.0,1.0,1.0},		-- range [0,1] 		: The RGB components of the specular light reflected by the material.
		["emissive"] 					= {0,0,0},					-- range [0,1] 		: The RGB components of the light emitted by the material.
		["alpha"]						= 1,						-- range [0,1] 		: The transparency of the material surface (0 fully transparent, 1 fully opaque).
		["shininess"] 					= 80,						-- range [0,128] 	: Determines the size and sharpness of specular highlights.
		["reflectivity"]				= 1,						-- range [0,1]		: Determines the amount of reflectivity.
		["metallic"]					= 0,						-- range [0,1]
		["roughness"]					= 0,						-- range [0,1]
		["glossiness"]					= 0,						-- range [0,1]
		
		["diffuseMap"]					= nil,
		["specularMap"]					= nil,
		["emissiveMap"] 				= nil,
		["alphaMap"]					= nil,
		["occulsionMap"] 				= nil,
		["normalMap"]					= nil,
		["metallicMap"]					= nil,
		["roughnessMap"]				= nil,
		["glossinessMap"]				= nil,
		
		["lineWidth"] 					= 1,						-- range [0,100]
		["pointSize"]					= 1,						-- range [0,100]
		["alphaMode"]					= "blend",					-- "opaque", "blend", "mask"
		["alphaCutoff"]					= 0.5,						-- range [0,1]
		["twosided"]					= false,					-- Whether to render geometry backfaces.
		["winding"]						= "ccw",					-- "ccw", "cw"
		["wireframe"] 					= false,
		["wireframeLineWidth"]			= 1							-- range [0,100]
	}
	
	-- if (type(fname) == "string") then
		-- local str,len = filesystem.read(fname)
		-- if (len == nil or len <= 0) then
			-- return
		-- end
		-- local status,result = xpcall(assert(loadstring(str,fname:sub(-45))),STP.stacktrace)
		-- if not (status) then
			-- print("[ERROR] failed to import scene " .. fname .. "\n" .. result)
			-- return
		-- end
		-- utils.table_merge(self.properties,result)
	-- elseif (type(fname) == "table") then
		-- utils.table_merge(self.properties,fname)
	-- end
end

-- getters and setters
function cMaterial:ambient(r,g,b,a)
	if (r or g or b or a) then
		self.properties.ambient[ 1 ] = math.clamp(r or 0,0,1)
		self.properties.ambient[ 2 ] = math.clamp(g or 0,0,1)
		self.properties.ambient[ 3 ] = math.clamp(b or 0,0,1)
		self.properties.ambient[ 4 ] = math.clamp(a or 0,0,1)
	end
	return unpack(self.properties.ambient)
end

function cMaterial:diffuse(r,g,b,a)
	if (r or g or b or a) then
		self.properties.diffuse[ 1 ] = math.clamp(r or 0,0,1)
		self.properties.diffuse[ 2 ] = math.clamp(g or 0,0,1)
		self.properties.diffuse[ 3 ] = math.clamp(b or 0,0,1)
		self.properties.diffuse[ 4 ] = math.clamp(a or 0,0,1)
	end
	return unpack(self.properties.diffuse)
end

function cMaterial:specular(r,g,b,a)
	if (r or g or b or a) then
		self.properties.specular[ 1 ] = math.clamp(r or 0,0,1)
		self.properties.specular[ 2 ] = math.clamp(g or 0,0,1)
		self.properties.specular[ 3 ] = math.clamp(b or 0,0,1)
		self.properties.specular[ 4 ] = math.clamp(a or 0,0,1)
	end
	return unpack(self.properties.specular)
end

function cMaterial:emissive(r,g,b)
	if (r or g or b) then
		self.properties.emissive[ 1 ] = math.clamp(r or 0,0,1)
		self.properties.emissive[ 2 ] = math.clamp(g or 0,0,1)
		self.properties.emissive[ 3 ] = math.clamp(b or 0,0,1)
	end
	return unpack(self.properties.emissive)
end

function cMaterial:alpha(v)
	if (v ~= nil) then
		self.properties.alpha = math.clamp(v,0,1)
	end
	return self.properties.alpha
end

function cMaterial:shininess(v)
	if (v ~= nil) then
		self.properties.shininess = math.clamp(v,0,128)
	end
	return self.properties.shininess
end

function cMaterial:reflectivity(v)
	if (v ~= nil) then
		self.properties.reflectivity = math.clamp(v,0,1)
	end
	return self.properties.reflectivity
end

function cMaterial:metallic(v)
	if (v ~= nil) then
		self.properties.metallic = math.clamp(v,0,1)
	end
	return self.properties.metallic
end

function cMaterial:roughness(v)
	if (v ~= nil) then
		self.properties.roughness = math.clamp(v,0,1)
	end
	return self.properties.roughness
end

function cMaterial:glossiness(v)
	if (v ~= nil) then
		self.properties.glossiness = math.clamp(v,0,1)
	end
	return self.properties.glossiness
end

function cMaterial:diffuseMap(v)
	if (v ~= nil) then
		self.properties.diffuseMap = core.framework.graphics.import.image(v)
	end
	return self.properties.diffuseMap
end

function cMaterial:specularMap(v)
	if (v ~= nil) then
		self.properties.specularMap = core.framework.graphics.import.image(v)
	end
	return self.properties.specularMap
end

function cMaterial:emissiveMap(v)
	if (v ~= nil) then
		self.properties.emissiveMap = core.framework.graphics.import.image(v)
	end
	return self.properties.emissiveMap
end

function cMaterial:alphaMap(v)
	if (v ~= nil) then
		self.properties.alphaMap = core.framework.graphics.import.image(v)
	end
	return self.properties.alphaMap
end

function cMaterial:occulsionMap(v)
	if (v ~= nil) then
		self.properties.occulsionMap = core.framework.graphics.import.image(v)
	end
	return self.properties.occulsionMap
end

function cMaterial:normalMap(v)
	if (v ~= nil) then
		self.properties.normalMap = core.framework.graphics.import.image(v)
	end
	return self.properties.normalMap
end


function cMaterial:metallicMap(v)
	if (v ~= nil) then
		self.properties.metallicMap = core.framework.graphics.import.image(v)
	end
	return self.properties.metallicMap
end


function cMaterial:roughnessMap(v)
	if (v ~= nil) then
		self.properties.roughnessMap = core.framework.graphics.import.image(v)
	end
	return self.properties.roughnessMap
end

function cMaterial:glossinessMap(v)
	if (v ~= nil) then
		self.properties.glossinessMap = core.framework.graphics.import.image(v)
	end
	return self.properties.glossinessMap
end

function cMaterial:lineWidth(v)
	if (v ~= nil) then
		self.properties.lineWidth = math.clamp(v,0,1)
	end
	return self.properties.lineWidth
end

function cMaterial:pointSize(v)
	if (v ~= nil) then
		self.properties.pointSize = math.clamp(v,0,1)
	end
	return self.properties.pointSize
end

function cMaterial:alphaMode(v)
	if (v == "blend" or v == "opaque" or v == "mask") then
		self.properties.alphaMode = v
	end
	return self.properties.alphaMode
end

function cMaterial:alphaCutoff(v)
	if (v ~= nil) then
		self.properties.alphaCutoff = math.clamp(v,0,1)
	end
	return self.properties.alphaCutoff
end

function cMaterial:winding(v)
	if (v == "ccw" or v == "cw") then
		self.properties.winding = v
	end
	return self.properties.winding
end

function cMaterial:twosided(b)
	if (b ~= nil) then
		self.properties.twosided = b == true
	end
	return self.properties.twosided
end

function cMaterial:wireframe(b)
	if (b ~= nil) then
		self.properties.wireframe = b == true
	end
	return self.properties.wireframe
end

function cMaterial:wireframeWidth(v)
	if (v ~= nil) then
		self.properties.wireframeLineWidth = math.clamp(v,0,100)
	end
	return self.properties.wireframeLineWidth
end