local gfx = core.framework.graphics

-- core.framework.graphics.import.framebuffer(width,height)
getmetatable(this).__call = function (self,width,height)
	return cFrameBuffer(width,height)
end

------------------------------------------------------------------------
-- cFrameBuffer
------------------------------------------------------------------------
Class "cFrameBuffer"
function cFrameBuffer:__init(width, height)
	if (not width and not height) then
		width, height = gfx.getSize()
	end

	self.width  = width
	self.height = height

	self.framebuffer = ffi.new("GLuint[1]")
	GL.glGenFramebuffers(1, self.framebuffer)
	gfx.setFramebuffer(self)
	
	-- render buffers
	self.colorRenderbuffer = ffi.new("GLuint[1]")
	GL.glGenRenderbuffers(1, self.colorRenderbuffer)
	GL.glBindRenderbuffer(GL.GL_RENDERBUFFER, self.colorRenderbuffer[0])
	GL.glRenderbufferStorage(GL.GL_RENDERBUFFER, GL.GL_RGBA8, width, height)
	GL.glFramebufferRenderbuffer(GL.GL_FRAMEBUFFER, GL.GL_COLOR_ATTACHMENT0, GL.GL_RENDERBUFFER, self.colorRenderbuffer[0])

	self.depthRenderbuffer = ffi.new("GLuint[1]")
	GL.glGenRenderbuffers(1, self.depthRenderbuffer)
	GL.glBindRenderbuffer(GL.GL_RENDERBUFFER, self.depthRenderbuffer[0])
	GL.glRenderbufferStorage(GL.GL_RENDERBUFFER, GL.GL_DEPTH_COMPONENT16, width, height)
	GL.glFramebufferRenderbuffer(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT, GL.GL_RENDERBUFFER, self.depthRenderbuffer[0])
	
	-- Render-to-texture
	self.texture = ffi.new("GLuint[1]")
	GL.glGenTextures(1, self.texture)
	GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture[0])

	GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_BASE_LEVEL, 0)
	GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
	GL.glTexImage2D(GL.GL_TEXTURE_2D,0,GL.GL_RGBA,width,height,0,GL.GL_RGBA,GL.GL_UNSIGNED_BYTE,nil)
	GL.glFramebufferTexture2D(GL.GL_FRAMEBUFFER,GL.GL_COLOR_ATTACHMENT0,GL.GL_TEXTURE_2D,self.texture[0],0)

	gfx.clear()
	gfx.setFramebuffer()
end

function cFrameBuffer:draw(x, y, r, sx, sy, ox, oy, kx, ky)
	local gfx = core.framework.graphics
	local vao = gfx.getDefaultVAO()
	GL.glBindVertexArray(vao[0])
	
	local mode = gfx.transform.getMatrixMode()
	gfx.transform.setMatrixMode("model")
	gfx.transform.push()
	
		sx = sx or 1
		sy = sy or 1
		if (x or y) then
			gfx.transform.translate(x or 0,y or 0,0)
		end
		if (r) then
			gfx.transform.rotateZ(r)
		end
		gfx.transform.scale(self.width*sx,self.height*sy)
		
		GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture[0])

		local fh, fv
		if (self.opts) then
			fh, fv = self.opts.flipH, self.opts.flipV
		end	
		gfx.primitive.rect_uv:draw(nil,fh,fv)
		
	gfx.transform.pop()
	gfx.transform.setMatrixMode(mode)
	
	GL.glBindVertexArray(0)
end

function cFrameBuffer:getTexture()
	return self.texture or gfx.getDefaultTexture()
end

function cFrameBuffer:getRegion()
	return (self.x or 0),(self.y or 0),(self.width or 0),(self.height or 0)
end

function cFrameBuffer:getTextureSize()
	return (self.width or 0),(self.height or 0)
end

function cFrameBuffer:__gc()
	GL.glDeleteTextures(1, self.texture)
	GL.glDeleteRenderbuffers(1, self.colorRenderbuffer)
	GL.glDeleteRenderbuffers(1, self.depthRenderbuffer)
	GL.glDeleteFramebuffers(1, self.framebuffer)
end