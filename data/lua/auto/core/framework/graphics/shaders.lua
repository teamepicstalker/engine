_shader = nil

cache = {}				-- Shader program gluint by alias
shaderToAlias = {}		-- Find alias of a shader program by it's value
script = {} 			-- Cache for optional shader program script environments
submodules = {}

local function getShaderCompileStatus(shader)
	local status = ffi.new("GLint[1]")
	GL.glGetShaderiv(shader, GL.GL_COMPILE_STATUS, status)
	if (status[0] ~= GL.GL_FALSE) then
		return
	end

	local length = ffi.new("GLint[1]")
	GL.glGetShaderiv(shader, GL.GL_INFO_LOG_LENGTH, length)
	if (length[0] > 0) then
		local buffer = ffi.new("char[?]", length[0])
		GL.glGetShaderInfoLog(shader, length[0], nil, buffer)
		GL.glDeleteShader(shader)
		error(ffi.string(buffer, length[0]),4)
	else
		GL.glDeleteShader(shader)
		error("Failed to compile shader",4)
	end
end

local function createShader(type, source)
	local shader = GL.glCreateShader(type)
	source = ffi.new("char[?]", #source, source)
	local str = ffi.new("const char *[1]", source)
	GL.glShaderSource(shader, 1, str, nil)
	GL.glCompileShader(shader)

	getShaderCompileStatus(shader)
	return shader
end

function on_game_start()
	CallbackSet("framework_load",on_load,999) -- high priority
end

function on_load()
	-- autoload shaders
	
	local xml = core.framework.import.xml():parse("configs/shaders.xml")
	for i,element in ipairs(xml.data.xml[ 1 ].shader) do
		local alias = element[ 1 ].name
		if (cache[ alias ]) then 
			print("shader redifinition!",element[ 1 ].name)
			return
		end
		
		local shader = GL.glCreateProgram()
		shaderToAlias[ shader ] = alias
		cache[ alias ] = shader
		submodules[ shader ] = {}

		-- attached fragment shader
		local fname = element[ 1 ].fragment
		if (fname and filesystem.exists(fname)) then
			local fragmentSource = filesystem.read(fname)
			local fragmentShader = createShader(GL.GL_FRAGMENT_SHADER,fragmentSource)
			GL.glAttachShader(shader,fragmentShader)
		end

		-- attached vertex shader
		fname = element[ 1 ].vertex
		if (fname and filesystem.exists(fname)) then
			local vertexSource = filesystem.read(fname)
			local vertexShader = createShader(GL.GL_VERTEX_SHADER,vertexSource)
			GL.glAttachShader(shader,vertexShader)
		end

		-- detached fragment shaders only for this specific program
		if (element.fragment) then
			for j,v in ipairs(element.fragment) do
				if (v[ 2 ] and filesystem.exists(v[ 2 ])) then
					local fragmentSource = filesystem.read(v[ 2 ])
					local fragmentShader = createShader(GL.GL_FRAGMENT_SHADER,fragmentSource)
					submodules[ shader ][ v[ 1 ].name ] = fragmentShader
					-- print(1)
					-- GL.glAttachShader(shader,fragmentShader)
					-- print(2)
				end
			end
		end

		-- detached vertex shaders only for this specific program
		if (element.vertex) then
			for j,v in ipairs(element.vertex) do
				if (v[ 2 ] and filesystem.exists(v[ 2 ])) then
					local vertexSource = filesystem.read(v[ 2 ])
					local vertexShader = createShader(GL.GL_VERTEX_SHADER,vertexSource)
					submodules[ shader ][ v[ 1 ].name ] = vertexShader
				end
			end
		end

		GL.glLinkProgram(shader)
		
		-- Shaders can have optional script
		if (element[ 1 ].script and filesystem.exists(element[ 1 ].script)) then
			local chunk,err = filesystem.loadfile(element[ 1 ].script)
			if (chunk ~= nil) then
				local env = {}
			
				setmetatable(env,{__index=function(t,k)
					return rawget(_G,k) -- ability to access _G
				end})
				
				setfenv(chunk,env)
				
				local status,err = xpcall(chunk,STP.stacktrace)
				if (status) then
					print("loaded shader script: " .. element[ 1 ].script)
					env._shader = shader
					env.init(shader)
					script[ alias ] = env
				else
					print(err,debug.traceback(1))
				end
			else
				print(err,debug.traceback(1))
			end
		end
	end
	
	useProgram("default2D")
end

function getActive()
	return _shader
end

function setActive(shader)
	if (_shader ~= shader) then
		GL.glUseProgram(shader)
		_shader = shader
	end
end

function useProgram(alias)
	local shader = cache[ alias ]
	if not (shader) then 
		print("setActive shader doesn't exist!",alias)
		return 
	end
	setActive(shader)
end

function getProgram(alias)
	return cache[ alias ]
end

function attach(name)
	if (submodules[ _shader ][ name ]) then
		print("attach")
		GL.glAttachShader(_shader,submodules[ _shader ][ name ])
	end
end

function detach(name)
	if (submodules[ _shader ][ name ]) then
		GL.glDetachShader(_shader,submodules[ _shader ][ name ])
	end
end

-- https://forums.khronos.org/showthread.php/77574-Speed-of-glGetUniformLocation
local uniformCache = {}
local attribCache = {}

function getUniformLocation(name)
	local alias = shaderToAlias[ _shader ]
	if not (uniformCache[ alias ]) then
		uniformCache[ alias ] = {}
	end
	if not (uniformCache[ alias ][ name ]) then
		uniformCache[ alias ][ name ] = GL.glGetUniformLocation(_shader,name)
	end
	return uniformCache[ alias ][ name ]
end

function getAttribLocation(name)
	local alias = shaderToAlias[ _shader ]
	if not (attribCache[ alias ]) then
		attribCache[ alias ] = {}
	end
	if not (attribCache[ alias ][ name ]) then
		attribCache[ alias ][ name ] = GL.glGetAttribLocation(_shader,name)
	end
	return attribCache[ alias ][ name ]
end