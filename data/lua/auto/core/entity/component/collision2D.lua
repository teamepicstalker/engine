-- core.entity.component.collision2D()
getmetatable(this).__call = function (self,entity)
	local t = {
		dependencies			= {"spatial"},
		entity					= entity,
		boundingBox				= boundingBox,
		inside					= inside,
		useImageSize			= true,
		x						= 0,
		y						= 0,
		w 						= 0,
		h						= 0
	}
	return t
end

function boundingBox(self)
	local w,h = self.w,self.h
	if (self.useImageSize and self.entity.component.sprite) then
		w,h = self.entity.component.sprite:getTextureSize()
	end
	local x,y = unpack(self.entity.component.spatial:position())
	x = x + self.x
	y = y + self.y
	return x-w/2,y-h/2,w,h
end

function inside(self,x,y)
	local x2,y2,w,h = self:boundingBox()
	return x >= x2 and y >= y2 and x <= x2+w and y <= y2+h
end
-------------------------------------
-- Editor controls
-------------------------------------
function editor_controls(entity,component,ctx,node,data,depth)
	component.useImageSize = nuklear.nk_check_label(ctx,"Use Image Size",not component.useImageSize) == 0
	
	local size = ffi.new("struct nk_vec2")
	size.x = 300 -- w
	size.y = 200 -- h
	if (nuklear.nk_combo_begin_label(ctx,"BoundingBox",size) == 1) then
		nuklear.nk_layout_row_dynamic(ctx,25,1)
		local x,y,w,h = component.x,component.y,component.w,component.h
		x = nuklear.nk_propertyf(ctx, "#X:", -99999, x, 99999, 0.1,1)
		y = nuklear.nk_propertyf(ctx, "#Y:", -99999, y, 99999, 0.1,1)
		w = nuklear.nk_propertyf(ctx, "#Width:", 0, w, 99999, 1,1)
		h = nuklear.nk_propertyf(ctx, "#Height:", 0, h, 99999, 1,1)
		component.x = x
		component.y = y
		component.w = w
		component.h = h
		nuklear.nk_combo_end(ctx)
	end
end

function entity_editor_submit(entity,component,ctx,node,data,depth)

end