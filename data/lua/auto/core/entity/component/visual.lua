local gfx = core.framework.graphics

-- core.entity.component.visual()
getmetatable(this).__call = function (self,entity)
	return cModelManager(entity)
end

----------------------------------
-- attributes
----------------------------------
Class "cModelManager"
function cModelManager:__init(entity)
	self.dependencies = {"spatial"}
	self.entity = entity
	self.properties = { visible=true, textures={}, shaders={} }
end

function cModelManager:visible(b)
	if (b ~= nil) then
		self.properties.visible = b
	end
	return self.properties.visible
end

function cModelManager:model(path)
	if (path == nil) then
		return self.properties.model
	end
	if (path ~= "" and filesystem.exists(path) and not filesystem.isDirectory(path)) then
		if (self.properties.model and self.properties.model.name == path) then 
			return
		end
		self.properties.model = gfx.import.model(path,{"PreTransformVertices","OptimizeMeshes","CalcTangentSpace","Triangulate","FlipUVs"})
	else
		self.properties.model = nil
	end
	return self.properties.model
end

function cModelManager:update(dt,mx,my)

end

function cModelManager:draw()
	if (self.properties.visible ~= true or self.properties.model == nil) then 
		return
	end
 	gfx.transform.push()
		local position = self.entity.component.spatial:position()
		local orientation = self.entity.component.spatial:orientation()
		
		gfx.transform.translate(unpack(position))
		gfx.transform.rotate(orientation)
		
		self.properties.model:draw(self.properties.textures,self.properties.shaders)
		
	gfx.transform.pop()
end

function cModelManager:drawBBox()
	if (self.properties.visible ~= true or self.properties.model == nil) then 
		return
	end
	gfx.transform.push()
		local position = self.entity.component.spatial:position()
		local orientation = self.entity.component.spatial:orientation()
		
		gfx.transform.translate(unpack(position))
		gfx.transform.rotate(orientation)
		
		self.properties.model:drawBBox(gfx.color.Red)
		
	gfx.transform.pop()
end

function cModelManager:getBoundingBox()
	if (self.properties.model) then
		return self.properties.model.bbox.min,self.properties.model.bbox.max
	end
	return {0,0,0},{0,0,0}
end

function cModelManager:stateWrite(data)
	data.visible = self.properties.visible
	data.path = self.properties.model and self.properties.model.name or nil
	if (self.properties.model and self.properties.model.meshes) then
		data.textures = {}
		data.shaders = {}
		for i,oMesh in ipairs(self.properties.model.meshes) do
			data.textures[i] = {}
			for name,image in pairs(oMesh.textures) do
				data.textures[i][name] = image.name
			end
			data.shaders[i] = oMesh.shader
		end
	end
end 

function cModelManager:stateRead(data)
	self:model(data.path)
	self.properties.visible = data.visible
	self.properties.textures = {}
	if (data.textures) then
		
		for i,t in pairs(data.textures) do
			self.properties.textures[ i ] = {}
			for name,path in pairs(t) do
				self.properties.textures[ i ][ name ] = core.framework.graphics.import.image(path)
			end
		end
	end
	self.properties.shaders = {}
	if (data.shaders) then
		for i,v in ipairs(data.shaders) do
			self.properties.shaders[i] = v
		end
	end
end

-------------------------------------
-- Editor controls
-------------------------------------
function editor_controls(entity,component,ctx,node,data,depth)
	local p = data.tree_components
	if not (p.__node) then
		p.__node = {}
	end
	
	-- Visible
	component.properties.visible = nuklear.nk_check_label(ctx,"Visible",not component.properties.visible) == 0
	
	nuklear.nk_layout_row_dynamic(ctx,28,2)	
	
	-- Model
	if not (p.__node.model_path_label) then
		p.__node.model_path_label = {id="editor_controls_model__model_path_label",flags="NK_TEXT_ALIGN_LEFT",__content="st_model"}
	end
	core.interface.api.label(ctx,p.__node.model_path_label,data,depth)
	if not (p.__node.model_path_edit) then
		p.__node.model_path_edit = {id="editor_controls_model__model_path_edit",max=1024,filter="nk_filter_default",flags="NK_EDIT_BOX",__content=(component.properties.model and component.properties.model.name or "")}
	end
	if (core.interface.api.edit_string(ctx,p.__node.model_path_edit,data,depth) ~= nil) then
		local model_path = ffi.string(data[ p.__node.model_path_edit.id ].text)
		component:model(model_path)
	end

		
	if (component.properties.model and component.properties.model.meshes) then
		for i,oMesh in ipairs(component.properties.model.meshes) do
			-- Mesh Texture
			for name,image in pairs(oMesh.textures) do
				if not (p.__node["texture_path_label_"..name.."_"..i]) then
					p.__node["texture_path_label_"..name.."_"..i] = {id="editor_controls_model__texture_path_label_"..name.."_"..i,flags="NK_TEXT_ALIGN_LEFT",__content=name}
				end
				core.interface.api.label(ctx,p.__node["texture_path_label_"..name.."_"..i],data,depth)
				if not (p.__node["texture_path_edit_"..name.."_"..i]) then
					local image_path = component.properties.textures and component.properties.textures[i] and component.properties.textures[i][name] and component.properties.textures[i][name].name or image and image.name or ""
					p.__node["texture_path_edit_"..name.."_"..i] = {id="editor_controls_model__texture_path_edit_"..name.."_"..i,max=1024,filter="nk_filter_default",flags="NK_EDIT_BOX",__content=image_path}
				end
				if (core.interface.api.edit_string(ctx,p.__node["texture_path_edit_"..name.."_"..i],data,depth) ~= nil) then
					local path = ffi.string(data[ p.__node["texture_path_edit_"..name.."_"..i].id ].text)
					if (path ~= "" and filesystem.exists(path) and not filesystem.isDirectory(path)) then
						if not (component.properties.textures[i]) then
							component.properties.textures[i] = {}
						end
						component.properties.textures[i][name] = core.framework.graphics.import.image(path)
					end
				end
			end
			-- Mesh Shader
			if not (p.__node["shader_label_"..i]) then
				p.__node["shader_label_"..i] = {id="editor_controls_model__shader_label_"..i,flags="NK_TEXT_ALIGN_LEFT",__content="st_shader"}
			end
			core.interface.api.label(ctx,p.__node["shader_label_"..i],data,depth)
			if not (p.__node["shader_edit_"..i]) then
				local shader_name = (component.properties.shaders and component.properties.shaders[i] or oMesh.shader or "default3D")
				p.__node["shader_edit_"..i] = {id="editor_controls_model__shader_edit_"..i,max=1024,filter="nk_filter_default",flags="NK_EDIT_BOX",__content=shader_name}
			end
			if (core.interface.api.edit_string(ctx,p.__node["shader_edit_"..i],data,depth) ~= nil) then
				local shader = ffi.string(data[ p.__node["shader_edit_"..i].id ].text)
				if (core.framework.graphics.shaders.getProgram(shader)) then
					component.properties.shaders[i] = shader
				end
			end
		end
	end
end

function entity_editor_submit(entity,component,ctx,node,data,depth)

end