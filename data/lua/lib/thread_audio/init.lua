ffi 			= require("ffi")
Class			= require("class")
AL 				= require("openal") 		; if not (AL) then error("failed to load libopenal") end
vorbis 			= require("libvorbis") 		; if not (vorbis) then error("failed to load libvorbis") end
physfs 			= require("physicsfs") 		; if not (physfs) then error("failed to load phsyicsfs") end
kazmath			= require("kazmath")		; if not (kazmath) then error("failed to load kazmath") end

require("global")	-- printf, printe
require("thread_audio.source")
require("thread_audio.bitstream")
require("thread_audio.manager")

ALSoundManager = cSoundManager()
if not (ALSoundManager.initialized) then
	ALSoundManager = nil
end

require("thread_audio.communication")
require("thread_audio.main")