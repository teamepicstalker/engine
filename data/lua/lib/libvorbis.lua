local ffi = require( "ffi" )

-- Contains libogg, libvorbis, libvorbisenc and libvorbisfile bindings
ffi.cdef[[
typedef int64_t ogg_int64_t;
typedef struct {
  void *iov_base;
  size_t iov_len;
} ogg_iovec_t;
typedef struct {
  long endbyte;
  int endbit;
  unsigned char *buffer;
  unsigned char *ptr;
  long storage;
} oggpack_buffer;
typedef struct {
  unsigned char *header;
  long header_len;
  unsigned char *body;
  long body_len;
} ogg_page;
typedef struct {
  unsigned char *body_data;
  long body_storage;
  long body_fill;
  long body_returned;
  int *lacing_vals;
  ogg_int64_t *granule_vals;
  long lacing_storage;
  long lacing_fill;
  long lacing_packet;
  long lacing_returned;
  unsigned char header[282];
  int header_fill;
  int e_o_s;
  int b_o_s;
  long serialno;
  long pageno;
  ogg_int64_t packetno;
  ogg_int64_t granulepos;
} ogg_stream_state;
typedef struct {
  unsigned char *packet;
  long bytes;
  long b_o_s;
  long e_o_s;
  ogg_int64_t granulepos;
  ogg_int64_t packetno;
} ogg_packet;
typedef struct {
  unsigned char *data;
  int storage;
  int fill;
  int returned;
  int unsynced;
  int headerbytes;
  int bodybytes;
} ogg_sync_state;
void oggpack_writeinit(oggpack_buffer *b);
int oggpack_writecheck(oggpack_buffer *b);
void oggpack_writetrunc(oggpack_buffer *b,long bits);
void oggpack_writealign(oggpack_buffer *b);
void oggpack_writecopy(oggpack_buffer *b,void *source,long bits);
void oggpack_reset(oggpack_buffer *b);
void oggpack_writeclear(oggpack_buffer *b);
void oggpack_readinit(oggpack_buffer *b,unsigned char *buf,int bytes);
void oggpack_write(oggpack_buffer *b,unsigned long value,int bits);
long oggpack_look(oggpack_buffer *b,int bits);
long oggpack_look1(oggpack_buffer *b);
void oggpack_adv(oggpack_buffer *b,int bits);
void oggpack_adv1(oggpack_buffer *b);
long oggpack_read(oggpack_buffer *b,int bits);
long oggpack_read1(oggpack_buffer *b);
long oggpack_bytes(oggpack_buffer *b);
long oggpack_bits(oggpack_buffer *b);
unsigned char *oggpack_get_buffer(oggpack_buffer *b);
void oggpackB_writeinit(oggpack_buffer *b);
int oggpackB_writecheck(oggpack_buffer *b);
void oggpackB_writetrunc(oggpack_buffer *b,long bits);
void oggpackB_writealign(oggpack_buffer *b);
void oggpackB_writecopy(oggpack_buffer *b,void *source,long bits);
void oggpackB_reset(oggpack_buffer *b);
void oggpackB_writeclear(oggpack_buffer *b);
void oggpackB_readinit(oggpack_buffer *b,unsigned char *buf,int bytes);
void oggpackB_write(oggpack_buffer *b,unsigned long value,int bits);
long oggpackB_look(oggpack_buffer *b,int bits);
long oggpackB_look1(oggpack_buffer *b);
void oggpackB_adv(oggpack_buffer *b,int bits);
void oggpackB_adv1(oggpack_buffer *b);
long oggpackB_read(oggpack_buffer *b,int bits);
long oggpackB_read1(oggpack_buffer *b);
long oggpackB_bytes(oggpack_buffer *b);
long oggpackB_bits(oggpack_buffer *b);
unsigned char *oggpackB_get_buffer(oggpack_buffer *b);
int ogg_stream_packetin(ogg_stream_state *os, ogg_packet *op);
int ogg_stream_iovecin(ogg_stream_state *os, ogg_iovec_t *iov,int count, long e_o_s, ogg_int64_t granulepos);
int ogg_stream_pageout(ogg_stream_state *os, ogg_page *og);
int ogg_stream_pageout_fill(ogg_stream_state *os, ogg_page *og, int nfill);
int ogg_stream_flush(ogg_stream_state *os, ogg_page *og);
int ogg_stream_flush_fill(ogg_stream_state *os, ogg_page *og, int nfill);
int ogg_sync_init(ogg_sync_state *oy);
int ogg_sync_clear(ogg_sync_state *oy);
int ogg_sync_reset(ogg_sync_state *oy);
int ogg_sync_destroy(ogg_sync_state *oy);
int ogg_sync_check(ogg_sync_state *oy);
char *ogg_sync_buffer(ogg_sync_state *oy, long size);
int ogg_sync_wrote(ogg_sync_state *oy, long bytes);
long ogg_sync_pageseek(ogg_sync_state *oy,ogg_page *og);
int ogg_sync_pageout(ogg_sync_state *oy, ogg_page *og);
int ogg_stream_pagein(ogg_stream_state *os, ogg_page *og);
int ogg_stream_packetout(ogg_stream_state *os,ogg_packet *op);
int ogg_stream_packetpeek(ogg_stream_state *os,ogg_packet *op);
int ogg_stream_init(ogg_stream_state *os,int serialno);
int ogg_stream_clear(ogg_stream_state *os);
int ogg_stream_reset(ogg_stream_state *os);
int ogg_stream_reset_serialno(ogg_stream_state *os,int serialno);
int ogg_stream_destroy(ogg_stream_state *os);
int ogg_stream_check(ogg_stream_state *os);
int ogg_stream_eos(ogg_stream_state *os);
void ogg_page_checksum_set(ogg_page *og);
int ogg_page_version(const ogg_page *og);
int ogg_page_continued(const ogg_page *og);
int ogg_page_bos(const ogg_page *og);
int ogg_page_eos(const ogg_page *og);
ogg_int64_t ogg_page_granulepos(const ogg_page *og);
int ogg_page_serialno(const ogg_page *og);
long ogg_page_pageno(const ogg_page *og);
int ogg_page_packets(const ogg_page *og);
void ogg_packet_clear(ogg_packet *op);

typedef struct vorbis_info{
	int version;
	int channels;
	long rate;
	long bitrate_upper;
	long bitrate_nominal;
	long bitrate_lower;
	long bitrate_window;
	void *codec_setup;
} vorbis_info;
typedef struct vorbis_dsp_state{
	int analysisp;
	vorbis_info *vi;
	float **pcm;
	float **pcmret;
	int pcm_storage;
	int pcm_current;
	int pcm_returned;
	int preextrapolate;
	int eofflag;
	long lW;
	long W;
	long nW;
	long centerW;
	ogg_int64_t granulepos;
	ogg_int64_t sequence;
	ogg_int64_t glue_bits;
	ogg_int64_t time_bits;
	ogg_int64_t floor_bits;
	ogg_int64_t res_bits;
	void *backend_state;
} vorbis_dsp_state;
typedef struct vorbis_block{
	float **pcm;
	oggpack_buffer opb;
	long lW;
	long W;
	long nW;
	int pcmend;
	int mode;
	int eofflag;
	ogg_int64_t granulepos;
	ogg_int64_t sequence;
	vorbis_dsp_state *vd;
	void *localstore;
	long localtop;
	long localalloc;
	long totaluse;
	struct alloc_chain *reap;
	long glue_bits;
	long time_bits;
	long floor_bits;
	long res_bits;
	void *internal;
} vorbis_block;
struct alloc_chain{
	void *ptr;
	struct alloc_chain *next;
};


typedef struct vorbis_comment{
	char **user_comments;
	int *comment_lengths;
	int comments;
	char *vendor;
} vorbis_comment;
void vorbis_info_init(vorbis_info *vi);
void vorbis_info_clear(vorbis_info *vi);
int vorbis_info_blocksize(vorbis_info *vi,int zo);
void vorbis_comment_init(vorbis_comment *vc);
void vorbis_comment_add(vorbis_comment *vc, const char *comment);
void vorbis_comment_add_tag(vorbis_comment *vc, const char *tag, const char *contents);
char *vorbis_comment_query(vorbis_comment *vc, const char *tag, int count);
int vorbis_comment_query_count(vorbis_comment *vc, const char *tag);
void vorbis_comment_clear(vorbis_comment *vc);
int vorbis_block_init(vorbis_dsp_state *v, vorbis_block *vb);
int vorbis_block_clear(vorbis_block *vb);
void vorbis_dsp_clear(vorbis_dsp_state *v);
double vorbis_granule_time(vorbis_dsp_state *v, ogg_int64_t granulepos);
const char *vorbis_version_string(void);
int vorbis_analysis_init(vorbis_dsp_state *v,vorbis_info *vi);
int vorbis_commentheader_out(vorbis_comment *vc, ogg_packet *op);
int vorbis_analysis_headerout(vorbis_dsp_state *v,
	vorbis_comment *vc,
	ogg_packet *op,
	ogg_packet *op_comm,
	ogg_packet *op_code);
float **vorbis_analysis_buffer(vorbis_dsp_state *v,int vals);
int vorbis_analysis_wrote(vorbis_dsp_state *v,int vals);
int vorbis_analysis_blockout(vorbis_dsp_state *v,vorbis_block *vb);
int vorbis_analysis(vorbis_block *vb,ogg_packet *op);
int vorbis_bitrate_addblock(vorbis_block *vb);
int vorbis_bitrate_flushpacket(vorbis_dsp_state *vd, ogg_packet *op);
int vorbis_synthesis_idheader(ogg_packet *op);
int vorbis_synthesis_headerin(vorbis_info *vi,vorbis_comment *vc, ogg_packet *op);
int vorbis_synthesis_init(vorbis_dsp_state *v,vorbis_info *vi);
int vorbis_synthesis_restart(vorbis_dsp_state *v);
int vorbis_synthesis(vorbis_block *vb,ogg_packet *op);
int vorbis_synthesis_trackonly(vorbis_block *vb,ogg_packet *op);
int vorbis_synthesis_blockin(vorbis_dsp_state *v,vorbis_block *vb);
int vorbis_synthesis_pcmout(vorbis_dsp_state *v,float ***pcm);
int vorbis_synthesis_lapout(vorbis_dsp_state *v,float ***pcm);
int vorbis_synthesis_read(vorbis_dsp_state *v,int samples);
long vorbis_packet_blocksize(vorbis_info *vi,ogg_packet *op);
int vorbis_synthesis_halfrate(vorbis_info *v,int flag);
int vorbis_synthesis_halfrate_p(vorbis_info *v);
enum {
	OV_FALSE             = -1,
	OV_EOF               = -2,
	OV_HOLE              = -3,
	OV_EREAD             = -128,
	OV_EFAULT            = -129,
	OV_EIMPL             = -130,
	OV_EINVAL            = -131,
	OV_ENOTVORBIS        = -132,
	OV_EBADHEADER        = -133,
	OV_EVERSION          = -134,
	OV_ENOTAUDIO         = -135,
	OV_EBADPACKET        = -136,
	OV_EBADLINK          = -137,
	OV_ENOSEEK           = -138,
};


int vorbis_encode_init(vorbis_info *vi,
	long channels,
	long rate,
	long max_bitrate,
	long nominal_bitrate,
	long min_bitrate);
int vorbis_encode_setup_managed(vorbis_info *vi,
	long channels,
	long rate,
	long max_bitrate,
	long nominal_bitrate,
	long min_bitrate);
int vorbis_encode_setup_vbr(vorbis_info *vi,
	long channels,
	long rate,
	float quality
	);
int vorbis_encode_init_vbr(vorbis_info *vi,
	long channels,
	long rate,
	float base_quality
	);
int vorbis_encode_setup_init(vorbis_info *vi);
int vorbis_encode_ctl(vorbis_info *vi,int number,void *arg);
struct ovectl_ratemanage_arg {
	int management_active;
	long bitrate_hard_min;
	long bitrate_hard_max;
	double bitrate_hard_window;
	long bitrate_av_lo;
	long bitrate_av_hi;
	double bitrate_av_window;
	double bitrate_av_window_center;
};
struct ovectl_ratemanage2_arg {
	int management_active;
	long bitrate_limit_min_kbps;
	long bitrate_limit_max_kbps;
	long bitrate_limit_reservoir_bits;
	double bitrate_limit_reservoir_bias;
	long bitrate_average_kbps;
	double bitrate_average_damping;
};
enum {
	OV_ECTL_RATEMANAGE2_GET = 0x14,
	OV_ECTL_RATEMANAGE2_SET = 0x15,
	OV_ECTL_LOWPASS_GET  = 0x20,
	OV_ECTL_LOWPASS_SET  = 0x21,
	OV_ECTL_IBLOCK_GET   = 0x30,
	OV_ECTL_IBLOCK_SET   = 0x31,
	OV_ECTL_COUPLING_GET = 0x40,
	OV_ECTL_COUPLING_SET = 0x41,
	OV_ECTL_RATEMANAGE_GET = 0x10,
	OV_ECTL_RATEMANAGE_SET = 0x11,
	OV_ECTL_RATEMANAGE_AVG = 0x12,
	OV_ECTL_RATEMANAGE_HARD = 0x13,
};

typedef struct FILE FILE;
typedef struct {
	size_t (*read_func) (void *ptr, size_t size, size_t nmemb, void *datasource);
	int (*seek_func) (void *datasource, ogg_int64_t offset, int whence);
	int (*close_func) (void *datasource);
	long (*tell_func) (void *datasource);
} ov_callbacks;
enum {
	NOTOPEN              = 0,
	PARTOPEN             = 1,
	OPENED               = 2,
	STREAMSET            = 3,
	INITSET              = 4,
};
typedef struct OggVorbis_File {
	void *datasource;
	int seekable;
	ogg_int64_t offset;
	ogg_int64_t end;
	ogg_sync_state oy;
	int links;
	ogg_int64_t *offsets;
	ogg_int64_t *dataoffsets;
	long *serialnos;
	ogg_int64_t *pcmlengths;
	vorbis_info *vi;
	vorbis_comment *vc;
	ogg_int64_t pcm_offset;
	int ready_state;
	long current_serialno;
	int current_link;
	double bittrack;
	double samptrack;
	ogg_stream_state os;
	vorbis_dsp_state vd;
	vorbis_block vb;
	ov_callbacks callbacks;
} OggVorbis_File;
int ov_clear(OggVorbis_File *vf);
int ov_fopen(const char *path,OggVorbis_File *vf);
int ov_open(FILE *f,OggVorbis_File *vf,const void *initial,long ibytes);
int ov_open_callbacks(void *datasource, OggVorbis_File *vf,const void *initial, long ibytes, ov_callbacks callbacks);
int ov_test(FILE *f,OggVorbis_File *vf,const void *initial,long ibytes);
int ov_test_callbacks(void *datasource, OggVorbis_File *vf,const void *initial, long ibytes, ov_callbacks callbacks);
int ov_test_open(OggVorbis_File *vf);
long ov_bitrate(OggVorbis_File *vf,int i);
long ov_bitrate_instant(OggVorbis_File *vf);
long ov_streams(OggVorbis_File *vf);
long ov_seekable(OggVorbis_File *vf);
long ov_serialnumber(OggVorbis_File *vf,int i);
ogg_int64_t ov_raw_total(OggVorbis_File *vf,int i);
ogg_int64_t ov_pcm_total(OggVorbis_File *vf,int i);
double ov_time_total(OggVorbis_File *vf,int i);
int ov_raw_seek(OggVorbis_File *vf,ogg_int64_t pos);
int ov_pcm_seek(OggVorbis_File *vf,ogg_int64_t pos);
int ov_pcm_seek_page(OggVorbis_File *vf,ogg_int64_t pos);
int ov_time_seek(OggVorbis_File *vf,double pos);
int ov_time_seek_page(OggVorbis_File *vf,double pos);
int ov_raw_seek_lap(OggVorbis_File *vf,ogg_int64_t pos);
int ov_pcm_seek_lap(OggVorbis_File *vf,ogg_int64_t pos);
int ov_pcm_seek_page_lap(OggVorbis_File *vf,ogg_int64_t pos);
int ov_time_seek_lap(OggVorbis_File *vf,double pos);
int ov_time_seek_page_lap(OggVorbis_File *vf,double pos);
ogg_int64_t ov_raw_tell(OggVorbis_File *vf);
ogg_int64_t ov_pcm_tell(OggVorbis_File *vf);
double ov_time_tell(OggVorbis_File *vf);
vorbis_info *ov_info(OggVorbis_File *vf,int link);
vorbis_comment *ov_comment(OggVorbis_File *vf,int link);
long ov_read_float(OggVorbis_File *vf,float ***pcm_channels,int samples, int *bitstream);
long ov_read_filter(OggVorbis_File *vf,void *buffer,int length,int bigendianp,int word,int sgned,int *bitstream,void (*filter)(float **pcm,long channels,long samples,void *filter_param),void *filter_param);
long ov_read(OggVorbis_File *vf,void *buffer,int length,int bigendianp,int word,int sgned,int *bitstream);
int ov_crosslap(OggVorbis_File *vf1,OggVorbis_File *vf2);
int ov_halfrate(OggVorbis_File *vf,int flag);
int ov_halfrate_p(OggVorbis_File *vf);
]]

return ffi.load("libvorbisfile")