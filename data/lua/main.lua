-------------------------------------------------------------
-- Copyright 2018
-------------------------------------------------------------
ORGANIZATION_IDENTITY = "ToadRiderStudios"
APPLICATION_IDENTITY = "GammaRay"
-------------------------------------------------------------
-- organize command line into global table
-------------------------------------------------------------
CommandLine = {}
for opt in string.gmatch(table.concat(arg," "),"%s*\"*([^-]*)%s*\"*") do
	local start = opt:find(" ")
	if (start) then
		CommandLine[ opt:sub(1,start-1) ] = opt:sub(start+1)
	else
		CommandLine[ opt ] = true
	end
end

-------------------------------------------------------------
-- Setup pathes
-------------------------------------------------------------
ffi = require("ffi")

if ( jit.os == "Windows" ) then
	-- Declare `SetDllDirectoryA`
	ffi.cdef[[
		int __stdcall SetDllDirectoryA(const char* lpPathName);
	]]

	-- Get working directory
	local execdir = arg[ 1 ]
	
	-- fix path by adding a final slash
	if (execdir:match("^.*()[\\/]") < execdir:len()) then
		execdir = execdir .. "\\"
	end
	
	CommandLine.execdir = execdir
	
	-- Set DLL directory
	ffi.C.SetDllDirectoryA(execdir .. "bin")

	-- lib and include pathes
	package.path = package.path  .. ";" .. execdir .. "data\\lua\\lib\\?.lua;"
	package.path = package.path  .. execdir .. "data\\lua\\?.lua;"
	package.path = package.path  .. execdir .. "data\\lua\\lib\\?\\init.lua;"
	package.path = package.path  .. execdir .. "?.lua;"
	
	package.cpath = package.cpath  .. ";" .. execdir .. "bin\\?.dll;"
	package.cpath = package.cpath  .. execdir .. "bin\\loadall.dll;"
else
	-- Get working directory
	local execdir = arg[ 1 ]
	
	-- fix path by adding a final slash
	if (execdir:match("^.*()[\\/]") < execdir:len()) then
		execdir = execdir .. "/"
	end

	CommandLine.execdir = execdir

	-- Add Windows LUA_LDIR paths
	local ldir = "./?.lua;!lua/?.lua;!lua/?/init.lua;"
	package.path = package.path:gsub("^%./%?%.lua;",ldir)
	package.path = package.path:gsub("!",execdir)

	-- lib and include pathes
	package.path = package.path  .. ";" .. execdir .. "data/lua/lib/?.lua;"
	package.path = package.path  .. execdir .. "data/lua/?.lua;"
	package.path = package.path  .. execdir .. "data/lua/lib/?/init.lua;"
	package.path = package.path  .. execdir .. "?.lua;"
	
	package.cpath = package.cpath .. ";" .. execdir .. "bin/?.so;"
	package.cpath = package.cpath .. execdir .. "bin/loadall.so"
end
-------------------------------------------------------------
-- Initialize third-party libraries into global namespaces
-------------------------------------------------------------
bit 			= require("bit")
SDL 			= require("sdl") 		; assert(SDL,"Failed to load SDL")
GL 				= require("opengl") 	; assert(GL,"Failed to load GL")
kazmath 		= require("kazmath")	; assert(kazmath,"Failed to load kazmath")
physfs			= require("physicsfs")	; assert(physfs,"Failed to load physfs")
Class			= require("class")
time 			= require("time")
nuklear 		= require("nuklear")
IL 				= require("devil")		; assert(IL,"Failed to load devIL")
IL.ilInit()
lanes			= require("lanes").configure()
STP 			= require("StackTracePlus")
-------------------------------------------------------------
-- Global utilities
-------------------------------------------------------------
require("global")

-------------------------------------------------------------
-- Initialize the filesystem (PhysicsFS)
-------------------------------------------------------------
VFS_Plugins = nil
require("filesystem")
do
	if not (filesystem.init("",true)) then 
		return
	end

	VFS_Plugins = require(CommandLine["vfs"] or "vfs") -- Can pass path through command line. (ie. -vfs vfs_custom.lua)
	for plugin,t in pairs(VFS_Plugins) do
		local path = t[3] and CommandLine.execdir .. t[1] or t[1]
		--print(path)
		filesystem.mount(path,t[2] or "/",false)
	end
	
	-- Mount appdata/user directory as write path
	local appdata_path = filesystem.getAppdataDirectory()
	filesystem.setWriteDirectory(appdata_path)
	filesystem.mkdir(ORGANIZATION_IDENTITY)
	filesystem.setWriteDirectory(appdata_path..ORGANIZATION_IDENTITY.."/")
	filesystem.mkdir(APPLICATION_IDENTITY)
	filesystem.setWriteDirectory(appdata_path..ORGANIZATION_IDENTITY.."/"..APPLICATION_IDENTITY.."/")
	filesystem.mount(appdata_path..ORGANIZATION_IDENTITY.."/"..APPLICATION_IDENTITY.."/","/",false)
end

-------------------------------------------------------------
--[[	Setup the environment
Here we compose a new lua environment for *.lua found in the 'auto' directory. Each directory and script becomes it's own table. This means, when a variable 
value doesn't exist, it checks namespace_bindings; it will try to auto load a *.lua. This is so individual scripts can reference eachother 
without load order issues, simply by autoloading them. It also eliminates the hassle of using module or require.
Examples:
lua/auto/core/framework/framework.lua will be executed within the _G.core.framework environment and can be accessed as this table.
lua/auto/core/framework/graphics can be accessed at core.framework.graphics
--]]
-------------------------------------------------------------
local namespace_bindings = {}

-- Iterate all *.lua in lua/auto directory
local function on_execute(path,fname,fullpath)
	local sname = fname:sub(1,-5)
	local t = {}
	local parent_namespace = "_G"
	for s in path:sub(10):gsplit("/",true) do
		if (s ~= "") then
			table.insert(t,s)
			if not (namespace_bindings[ s ]) then
				namespace_bindings[ s ] = {}
			end
			if not (namespace_bindings[ s ][ parent_namespace ]) then
				namespace_bindings[ s ][ parent_namespace ] = {}
			end
			if (sname ~= s) then -- if script name same as directory, then keep current parent ie. core/framework/framework.lua should be core.framework
				parent_namespace = parent_namespace .. "." .. s
			end
		end
	end
	if not (namespace_bindings[ sname ]) then
		namespace_bindings[ sname ] = {}
	end
	if not (namespace_bindings[ sname ][ parent_namespace ]) then
		namespace_bindings[ sname ][ parent_namespace ] = {}
	end
	namespace_bindings[ sname ][ parent_namespace ][ 1 ] = t
	namespace_bindings[ sname ][ parent_namespace ][ 2 ] = fullpath
end
filesystem.fileForEach("lua/auto",true,{"lua"},on_execute)

-- create the metatable structure for all scripts in auto/
_G.__namespace__ = "_G"
setmetatable(_G,{__index=function(t,k)
	if (k == "this") then 
		return t
	elseif (k == "Class") then
		setfenv(Class,t) -- So that this[name] in Class isn't always _G, but calling table
	end
	
	-- if variable name indexing this table exists in the binding table along with this tables namespace, then it has a script to load
	local root = namespace_bindings[ k ] and namespace_bindings[ k ][ t.__namespace__ ]
	if (root) then
		namespace_bindings[ k ][ t.__namespace__ ] = nil
		if not (root[1]) then
			local env = {}
			env.__namespace__ = t.__namespace__ .. "." .. k
			env.__holder__ = t
			local mt = {}
			local p_mt = getmetatable(t)
			-- copy metatable from parent
			for k, v in next, p_mt, nil do
				mt[k] = v
			end
			setmetatable(env,mt)
			
			rawset(t,k,env)
			return env
		else
			local node = _G
			for i,namespace in ipairs(root[ 1 ]) do 
				if (node[ namespace ]) then 
					node = node[ namespace ]
				end
			end
			if (node == _G or node == t) then
				local chunk,errormsg = filesystem.loadfile(root[ 2 ])
				if (chunk ~= nil) then
					local env = {}
					env.__namespace__ = t.__namespace__ .. "." .. k
					env.__holder__ = t
					local mt = {}
					local p_mt = getmetatable(t)
					-- copy metatable from parent
					for k, v in next, p_mt, nil do
						mt[k] = v
					end
					setmetatable(env,mt)
					
					setfenv(chunk,env)
					local status,err = xpcall(chunk,STP.stacktrace)
					if (status) then
						print("loaded script: " .. root[ 2 ])
					else
						print(err,debug.traceback(1))
					end
					
					rawset(t,k,env)
					return env
				else
					print(errormsg,debug.traceback(1))
				end
			end
		end
	end
	
	return rawget(_G,k)
end})
	
-- Run the application
core.framework.run()
