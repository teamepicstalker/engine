#version 330

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float u_pointSize;

in vec3 position;
in vec2 texcoord;

out vec2 TexCoord;

void main()
{
	TexCoord    = texcoord;
	gl_Position = projection * view * model * vec4(position.xy, 0.0, 1.0);
	gl_PointSize = u_pointSize;
}
