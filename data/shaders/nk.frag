#version 330

precision mediump float;

uniform sampler2D tex;
uniform vec4 color;

in vec2 TexCoord;
in vec4 Color;

layout(location = 0) out vec4 FragColor;

void main()
{
	FragColor = texture( tex, TexCoord.st ) * Color;
}
