#version 330

uniform sampler2D tex;
uniform vec4 color;

in vec2 TexCoord;

layout(location = 0) out vec4 FragColor;

void main()
{
	FragColor = texture( tex, TexCoord ) * color;
}
