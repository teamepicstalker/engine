#version 330

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float u_pointSize;

in vec2 position;
in vec2 texcoord;
in vec4 color;

out vec2 TexCoord;
out vec4 Color;

void main()
{
	TexCoord    = texcoord;
	Color		= color;
	gl_Position = projection * view * model * vec4(position, 0.0, 1.0);
	gl_PointSize = u_pointSize;
}
