#version 330

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float u_pointSize;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 tangent;
layout(location = 3) in vec3 bitangent;
layout(location = 4) in vec2 texcoord;

out vec3 Normal;
out vec3 Tangent;
out vec2 TexCoord;

void main()
{
	Normal      = normal;
	Tangent     = tangent;
	TexCoord    = texcoord;
	
	gl_Position = projection * view * model * vec4( position, 1.0 );
	gl_PointSize = u_pointSize;
}
