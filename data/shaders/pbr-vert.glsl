#version 330

#define HAS_NORMALS
#define HAS_TANGENTS
#define HAS_UV

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float u_pointSize;

layout(location = 0) in vec4 position;

#ifdef HAS_NORMALS
layout(location = 1) in vec4 normal;
#endif

#ifdef HAS_TANGENTS
layout(location = 2) in vec3 tangent;
layout(location = 3) in vec3 bitangent;
#endif

#ifdef HAS_UV
layout(location = 4) in vec2 texcoord;
#endif

out vec3 v_Position;
out vec2 v_UV;

#ifdef HAS_NORMALS
	#ifdef HAS_TANGENTS
		out mat3 v_TBN;
	#else
		out vec3 v_Normal;
	#endif
#endif


void main()
{
	vec4 pos = model * position;
	v_Position = vec3(pos.xyz) / pos.w;

#ifdef HAS_NORMALS
	#ifdef HAS_TANGENTS
		mat4 normalMatrix = inverse(model);
		vec3 normalW = normalize(vec3(normalMatrix * vec4(normal.xyz, 0.0)));
		vec3 tangentW = normalize(vec3(model * vec4(tangent.xyz, 0.0)));
		vec3 bitangentW = normalize(vec3(model * vec4(bitangent.xyz, 0.0)));//cross(normalW, tangentW) * tangent.w;
		v_TBN = mat3(tangentW, bitangentW, normalW);
	#else // HAS_TANGENTS != 1
		v_Normal = normalize(vec3(model * vec4(normal.xyz, 0.0)));
	#endif
#endif

#ifdef HAS_UV
	v_UV = texcoord;
#else
	v_UV = vec2(0.,0.);
#endif

	gl_Position = projection * view * model * position; // needs w for proper perspective correction
	gl_PointSize = u_pointSize;
}
